<?php

class ORMCompiler
{

	private Connection $connection;
	private ORMConfig  $config;

	private array $fk_tables;
	private array $m2m_tables;

	public function __construct( ORMConfig $config )
	{
		$this->config = $config;

		try
		{
			$this->connection = DBConnection::getConnection( $this->config );
		}
		catch (Exception $e)
		{
			exit( 'Datenbankverbindung fehlgeschlagen. Grund: ' . $e->getMessage() );
		}

		$this->updateTableStructure();
		$this->analyseSchema();
		$this->build();
	}

	private function updateTableStructure(): void
	{
		$myTables = $this->connection->getDatabaseInfo()->getTables();

		$this->connection->executeUpdate("SET foreign_key_checks = 0");

		// binary to uuid
		$this->connection->executeUpdate("DROP FUNCTION IF EXISTS `BIN_TO_UUID`");
		$this->connection->executeUpdate("
		CREATE FUNCTION `BIN_TO_UUID`(bin BINARY(16)) RETURNS char(36) CHARSET utf8mb4 COLLATE utf8mb4_general_ci
		    DETERMINISTIC
		BEGIN
			DECLARE hex CHAR(32);
			SET hex = HEX(bin);
			RETURN LOWER(CONCAT(LEFT(hex, 8), '-', MID(hex, 9, 4), '-', MID(hex, 13, 4), '-', MID(hex, 17, 4), '-', RIGHT(hex, 12)));
		END");

		// uuid to binary
		$this->connection->executeUpdate("DROP FUNCTION IF EXISTS `UUID_TO_BIN`");
		$this->connection->executeUpdate("
		CREATE FUNCTION `UUID_TO_BIN`(uuid CHAR(36)) RETURNS binary(16)
		    DETERMINISTIC
		BEGIN
			RETURN UNHEX(CONCAT(REPLACE(uuid, '-', '')));
		END");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
		// tstamp und userids hinzufügen (für status active/deleted etc) + Timestamps
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
		$new_columns = [ 'tstamp_created', 'tstamp_modified', 'tstamp_deleted', 'userid_created', 'userid_modified', 'userid_deleted' ];
		foreach ( $myTables as $table )
		{
			/**
			 * @var $table TableInfo
			 */
			$already = [];

			/**
			 * @var $primaryKeys PrimaryKeyInfo
			 */
			$primaryKeys = [];

			if( empty($table->getPrimaryKey()) )
				throw new Exception('no primary key found for table ' . $table->getName());


			$existing_indices = [];
			foreach ( $table->getIndices() as $index )
			{
				$idx_cols = $index->getColumns();
				if( count($idx_cols) > 1 )
					continue; // skip combined indices

				// add key to list
				if ( in_array($idx_cols[0]->getName(), $new_columns ) and $index->isUnique() != true )
					$existing_indices[$idx_cols[0]->getName()] = $idx_cols[0]->getName();
			}

			// add index for new columns
			foreach ($new_columns as $column)
			{
				if( isset($existing_indices[$column]) )
					continue; // skip existing index

				$tablename = $table->getName();
				$idx = 'IDX_' . strtoupper($tablename) . '_' . strtoupper($column);
                $this->connection->executeUpdate( "ALTER TABLE `$tablename` ADD INDEX `$idx` (`$column`);");
			}

			foreach ( $table->getPrimaryKey()->getColumns() as $column )
			{
				/**
				 * @var $column ColumnInfo
				 */
				$primaryKeys[$column->getName()] = $column;
			}

			$uuid_columns = [];
			foreach ( $table->getColumns() as $column )
			{
				/**
				 * @var $column ColumnInfo
				 */
				if ( in_array( $column->getName(), $new_columns ) )
				{
					$already[] = $column->getName();
				}

				// ist auto increment

				// convert all columns to UUIDs (bad performance on large tables)
				if( /*$column->isAutoIncrement() AND isset($primaryKeys[$column->getName()])*/ $column->getNativeType() == 'int' AND $column->getSize() == 11 )
				{
					// Change Field to Binary for UUID
					$this->connection->executeUpdate( 'ALTER TABLE `' . $table->getName() . '` CHANGE  `' . $column->getName() . '` `' . $column->getName() . '`' . " binary(16) " . ( $column->isNullable() ? 'NULL' : 'NOT NULL' ) );
				}

				// convert all UUIDs to INT11
				if( /*$column->isAutoIncrement() AND isset($primaryKeys[$column->getName()])*/ ( $column->getNativeType() == 'binary' AND $column->getSize() == 16 ) OR  ( $column->getNativeType() == 'int' AND $column->getSize() == 11 ) )
				{
					// Change Field to Binary for UUID
					//$this->connection->executeUpdate( 'ALTER TABLE `' . $table->getName() . '` CHANGE  `' . $column->getName() . '` `' . $column->getName() . '`' . " INT(11) unsigned " . ( $column->isNullable() ? 'NULL' : 'NOT NULL' ) .  ( isset($primaryKeys[$column->getName()]) ? ' AUTO_INCREMENT ' : '' ) );
				}

				if( $column->getNativeType() == 'binary' AND $column->getSize() == 16 )
					$uuid_columns[$column->getName()] = true;
			}

			// add columns (die noch fehlen)
			foreach ( $new_columns as $new )
			{
				if ( in_array( $new, $already ) )
					continue;

				switch ( $new )
				{
					case 'tstamp_created':
					case 'tstamp_modified':
					case 'tstamp_deleted':
						$sql = 'ALTER TABLE `' . $table->getName() . '` ADD COLUMN `' . $new . '` TIMESTAMP ';
						break;

					case 'userid_created':
					case 'userid_modified':
					case 'userid_deleted':
						if( isset($uuid_columns[$new]) )
							$sql = 'ALTER TABLE `' . $table->getName() . '` ADD COLUMN `' . $new . '` BINARY(16) ';
						else
							$sql = 'ALTER TABLE `' . $table->getName() . '` ADD COLUMN `' . $new . '` INT(11) UNSIGNED ';
						break;
				}

				switch ( $new )
				{
					case 'tstamp_created':
					case 'userid_created':
						$sql .= ' NOT NULL ';
						break;
					default:
						$sql .= ' NULL';
						break;
				}

				if ( $new == 'tstamp_created' )
					$sql .= ' DEFAULT CURRENT_TIMESTAMP';
				if ( $new == 'userid_created' AND !isset($uuid_columns[$new]) )
					$sql .= ' DEFAULT 1';

				try
				{
					$has_updates = true;
					echo "## DO TABLE Updates -> $sql" . PHP_EOL;
					$this->connection->executeUpdate( $sql );
					$this->connection->executeUpdate( 'ALTER TABLE `' . $table->getName() . '` ADD INDEX `' . $new . '` (`' . $new . '`)' );
				}
				catch (Exception $e)
				{
					var_dump( $e );
					die();
				}
			}
		}
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
	}

	private function analyseSchema(): void
	{
		$myTables = $this->connection->getDatabaseInfo()
			->getTables();
		$this->fk_tables = [];
		$this->m2m_tables = [];

		// Liste aller Tabellennamen (relevant für m2m Prüfung)
		$tableNameList = [];
		foreach ( $myTables as $table )
			$tableNameList[ preg_replace( '/^tbl_/ui', '', $table->getName() ) ] = $table;

		// Schema
		foreach ( $myTables as $table )
		{
			// @TODO prefix per Config einstellen
			$tablename = preg_replace( '/^tbl_/ui', '', $table->getName() );

			// TODO
			$rare_reference_2to1_self = FALSE;

			// TODO: das ist schmarrn - hier muss eigentlich eine bessere logik rein -> Es könnten auch mehr FKs in der Tabelle sein ..
			// auch MM-Tabellen sollten einen eindeutigen PK besitzen
			$count_pk = count( $table->getPrimaryKey()->getColumns() );
			$count_fk = count( $table->getForeignKeys() );

			// m2m wenn der PK zusammengesetzt ist. / Trifft aber nicht zu wenn laut Konvention ein eindeutiger PK (mit Autoinkrement) angelegt wurde
			$is_manytomany = ( $count_pk == 2 and $count_fk >= $count_pk );

			// m2m Prüfung über tabellennamen
			// - nur wenn min. 2 FK vorhanden sind und wenn im Tabellennamen ein Trennzeichen ist
			$pieces = explode( '_', $tablename );

			$m2m_required_tables = [];
			if ( $count_fk >= 2 and count( $pieces ) == 2 )
			{
				echo 'Table ' . $table->getName() . ' könnte eine m2m sein'  . PHP_EOL;

				$FKs_zum_pruefen = [];
				foreach ( $table->getForeignKeys() as $fk )
				{
					$references = $fk->getReferences();
					$ref_column = $references[0][1];
					$ref_tablename = preg_replace( '/^tbl_/ui', '', $ref_column->getTable()
						->getName() );

					// History Spalten ausschliessen
					if ( preg_match( '/^userid_(created|modified|deleted)/ui', $ref_column->getName() ) )
						continue;

					if ( isset( $tableNameList[ $ref_tablename ] ) )
						$m2m_required_tables[ $ref_column->getTable()
							->getName() ] = TRUE;
				}

				// prüfe die zusammensetzung des Tabellennamens - wenn beide Teile des Names (getrennt mit _) nicht vorkommen, kann es keine m2m sein
				// stimmt leider auch nicht. Siehe tbl_manager_friends
				$m2m_check_name_count = 0;
				foreach ( $pieces as $piece )
				{
					if ( isset( $tableNameList[ $piece ] ) )
						$m2m_check_name_count++;
				}

				// mit großer Wahrscheinlichkeit handelt es sich um eine m2m Tabelle
				if ( $m2m_check_name_count > 1 AND $m2m_check_name_count == count( $m2m_required_tables ) )
					$is_manytomany = TRUE;
			}

			if ( $is_manytomany )
				echo ':: m2m wurde ermittelt für: ' . $table->getName() . PHP_EOL;

			// mehrfach referenzen feststellen
			$myReferences = [];
			foreach ( $table->getForeignKeys() as $fk )
			{
				$references = $fk->getReferences();
				$ref_column = $references[0][1];
				$ref_table = $ref_column->getTable();

				if ( !isset( $myReferences[ $ref_table->getName() ] ) )
					$myReferences[ $ref_table->getName() ] = 0;

				$myReferences[ $ref_table->getName() ]++;
			}


			// es sind 2 pk und die fks zeigen auf die selbe tabelle - seltene selbstreferenz (z.B.: Freundesliste, eine person hat mehrere Freunde (assoziationen))
			// hier soll es sich um eine normale single-to-many referenz handeln -> workaround
			if ( $is_manytomany and count( $myReferences ) == 1 )
			{
				$rare_reference_2to1_self = TRUE; // 2 zu 1 auf sich selbst
				//continue;
			}

			if ( $is_manytomany )
			{
				// many to many
				$fks = $table->getForeignKeys();

				$ref1 = $fks[0]->getReferences();
				$ref2 = $fks[1]->getReferences();
				$cross_col1 = $fks[0]->getName();
				$cross_col2 = $fks[1]->getName();
				$ziel1 = $ref1[0][1]->getTable();
				$ziel2 = $ref2[0][1]->getTable();

				$myArray1 = [
					'table'                 => $ziel2->getName(),
					'referenz_name'         => ORMBuilderBase::makeClassName( $ziel2->getName() ),
					'referenz_name_org'     => str_replace( 'tbl_', '', strtolower( $ziel2->getName() ) ),
					'quelle_name'           => $ziel1->getName(),
					'quelle_name_org'       => str_replace( 'tbl_', '', strtolower( $ziel1->getName() ) ),
					'classname'             => ORMBuilderBase::makeClassName( $ziel2->getName() ),
					'quelle_classname'      => ORMBuilderBase::makeClassName( $ziel1->getName() ),
					'quelle_tablename'      => $ziel1->getName(),
					'col_ref'               => $ref2[0][1]->getName(),
					'col_ref_attributename' => ORMBuilderBase::formatAttributename( $ref2[0][1]->getName() ),
					'col_own'               => $ref1[0][1]->getName(),
					'col_own_attributename' => ORMBuilderBase::formatAttributename( $ref1[0][1]->getName() ),
					'cross_col'             => ORMBuilderBase::formatAttributename( $cross_col1 ),
					'cross_col_org'         => $cross_col1,
					'type'                  => DBReferenceTypes::REFERENCE_MANY_TO_MANY,
					'ref_table'             => $table->getName(),
					'assigned'              => TRUE,
					'nullable'              => FALSE,
				];
				// beide fks zeigen auf die selbe tabelle - namen ändern!
				if ( count( $myReferences ) == 1 )
				{
					// Eigenen Referenznamen anhand des Tabellennamens bestimmten
					// TODO: könnte auch über die referenzierte Spalte gehen . Zb. "managerid_sender" oder "parent_managerid"
					$custom_name = str_replace($myArray1['quelle_tablename'] . '_', '', $myArray1['ref_table']);
					//$custom_name = str_replace( $myArray1['col_own'] . '_', '', $myArray1['cross_col_org'] );
					$myArray1['referenz_name_org'] = $custom_name;
				}


				$this->fk_tables[ $ziel1->getName() ][] = $myArray1;

				$myArray2 = [
					'table'                 => $ziel1->getName(),
					'referenz_name'         => ORMBuilderBase::makeClassName( $ziel1->getName() ),
					'referenz_name_org'     => str_replace( 'tbl_', '', strtolower( $ziel1->getName() ) ),
					'quelle_name'           => $ziel2->getName(),
					'quelle_name_org'       => str_replace( 'tbl_', '', strtolower( $ziel2->getName() ) ),
					'classname'             => ORMBuilderBase::makeClassName( $ziel1->getName() ),
					'quelle_classname'      => ORMBuilderBase::makeClassName( $ziel2->getName() ),
					'quelle_tablename'      => $ziel2->getName(),
					'col_ref'               => $ref1[0][1]->getName(),
					'col_ref_attributename' => ORMBuilderBase::formatAttributename( $ref1[0][1]->getName() ),
					'col_own'               => $ref2[0][1]->getName(),
					'col_own_attributename' => ORMBuilderBase::formatAttributename( $ref2[0][1]->getName() ),
					'cross_col'             => ORMBuilderBase::formatAttributename( $cross_col2 ),
					'cross_col_org'         => $cross_col2,
					'type'                  => DBReferenceTypes::REFERENCE_MANY_TO_MANY,
					'ref_table'             => $table->getName(),
					'assigned'              => TRUE,
					'nullable'              => FALSE,
				];
				// beide fks zeigen auf die selbe tabelle - namen ändern!
				if ( count( $myReferences ) == 1 )
				{
					// alt
					//$myArray2['referenz_name_org'] = $myArray2['referenz_name_org'] . '_' . $myArray2['cross_col_org'];

					// Eigenen Referenznamen anhand des Tabellennamens bestimmten
					// TODO: könnte auch über die referenzierte Spalte gehen . Zb. "managerid_sender" oder "parent_managerid"
					$custom_name = str_replace($myArray1['quelle_tablename'] . '_', '', $myArray1['ref_table']);
					//$custom_name = str_replace( $myArray2['col_own'] . '_', '', $myArray2['cross_col_org'] );
					$myArray2['referenz_name_org'] = $custom_name;
				}


				$myArray2['referenz_name'] = ORMBuilderBase::makeClassName( $myArray2['referenz_name_org'] );

				$this->fk_tables[ $ziel2->getName() ][] = $myArray2;

				// m2m hinzufügen - verknüpfungstabelle braucht auch referenzierungen
				$myArray1['type'] = DBReferenceTypes::REFERENCE_SINGLE_TO_MANY;
				$myArray1['col_own'] = $myArray2['cross_col_org'];
				$myArray1['col_own_attributename'] = $myArray2['cross_col'];
				$myArray2['type'] = DBReferenceTypes::REFERENCE_SINGLE_TO_MANY;
				$myArray2['col_own'] = $myArray1['cross_col_org'];
				$myArray2['col_own_attributename'] = $myArray1['cross_col'];
				if ( !isset( $this->fk_tables[ $table->getName() ] ) )
					$this->fk_tables[ $table->getName() ] = [];
				$this->fk_tables[ $table->getName() ][] = $myArray1;

				if ( !$rare_reference_2to1_self )
					$this->fk_tables[ $table->getName() ][] = $myArray2;

				$this->m2m_tables[ $table->getName() ] = [
					$ziel1->getName(),
					$ziel2->getName(),
				];
			}

			else
			{
				// default - SINGLE-TO-MANY
				foreach ( $table->getForeignKeys() as $fk )
				{
					$references = $fk->getReferences();
					$ref_column = $references[0][1];
					$own_column = $references[0][0];

					$own_type = DBReferenceTypes::REFERENCE_SINGLE_TO_SINGLE;
					// type rausfinden
					$primarykey_cols = $ref_column->getTable()
						->getPrimaryKey()
						->getColumns();

					// ziel checken und selbst die referenz geben, ziel ist meisst "single" -> PK
					if ( count( $primarykey_cols ) == 1 and $ref_column->getName() == $primarykey_cols[0]->getName() )
						$own_type = DBReferenceTypes::REFERENCE_SINGLE_TO_SINGLE;

					$ref_table = $ref_column->getTable();

					// wenn die referenz mehrfach vorhanden ist - werden die referenz namen anhand des spaltennamens geändert - "managerid_sender, managerid_recipient"
					$referenz_name_own_org = $ref_table->getName();
					$referenz_name_ref_org = $table->getName();
					$referenz_name_own = ORMBuilderBase::makeClassName( ORMBuilderBase::makeRefName( $referenz_name_own_org ) );
					$referenz_name_ref = ORMBuilderBase::makeClassName( ORMBuilderBase::makeRefName( $referenz_name_ref_org ) );

					if ( $myReferences[ $ref_table->getName() ] > 1 )
					{
						$referenz_name = $own_column->getName();
						if ( strpos( $referenz_name, '_' ) )
							$referenz_name = substr( $referenz_name, strpos( $referenz_name, '_' ) + 1 );

						//var_dump($referenz_name, $table->getName());

						$referenz_name_own_org = $ref_table->getName() . '_' . $referenz_name;
						$referenz_name_ref_org = $table->getName() . '_' . $referenz_name;
						$referenz_name_own = ORMBuilderBase::makeClassName( $ref_table->getName() ) . ORMBuilderBase::makeClassName( $referenz_name );
						$referenz_name_ref = ORMBuilderBase::makeClassName( $table->getName() ) . ORMBuilderBase::makeClassName( $referenz_name );
					}

					//echo( $referenz_name_ref );

					// ziel referenz prüfen
					$ziel_type = DBReferenceTypes::REFERENCE_SINGLE_TO_MANY;
					// wenn fk auch ein pk ist - dann kanns nur single sein
					$pk_check = $table->getPrimaryKey()
						->getColumns();

					// wenn nur ein PK gesetzt ist und diese spalte mit der Referenz-spalte übereinstimmt, handelt es sich um "single"
					// TODO: was hab ich mir da fürn käs ausgedacht ....
					if ( $pk_check[0]->getName() == $ref_column->getName() and count( $pk_check ) == 1 )
						$ziel_type = DBReferenceTypes::REFERENCE_SINGLE_TO_SINGLE;

					// wenn fk ein unique ist - dann kanns nur single sein
					foreach ( $table->getIndices() as $index )
					{
						$idx_cols = $index->getColumns();
						if ( $idx_cols[0]->getName() == $ref_column->getName() and $index->isUnique() == TRUE )
							$ziel_type = DBReferenceTypes::REFERENCE_SINGLE_TO_SINGLE;
					}

					//var_dump($ziel_type, $pk_check[0]->getName(), $ref_column->getName());

					// selbst eine referenz geben
					// nur wenn ziel tabelle nicht eigene ist - Selbstreferenz
					if ( $table->getName() != $ref_table->getName() )
					{
						$myArray = [
							'table'                 => $ref_table->getName(),
							'referenz_name'         => $referenz_name_own,
							'referenz_name_org'     => str_replace( 'tbl_', '', strtolower( $referenz_name_own_org ) ),
							'quelle_name'           => $referenz_name_ref,
							'quelle_name_org'       => str_replace( 'tbl_', '', strtolower( $referenz_name_ref_org ) ),
							'classname'             => ORMBuilderBase::makeClassName( $ref_table->getName() ),
							'quelle_classname'      => ORMBuilderBase::makeClassName( $table->getName() ),
							'quelle_tablename'      => $table->getName(),
							'type'                  => $own_type,
							'type_ziel'             => $ziel_type,
							'col_ref'               => $ref_column->getName(),
							'col_ref_attributename' => ORMBuilderBase::formatAttributename( $ref_column->getName() ),
							'col_own'               => $own_column->getName(),
							'col_own_attributename' => ORMBuilderBase::formatAttributename( $own_column->getName() ),
							'ref_table'             => NULL,
							'assigned'              => TRUE,
							'nullable'              => $own_column->isNullable(),
						];
						$this->fk_tables[ $table->getName() ][] = $myArray;
					}

					$myArray = [
						'table'                 => $table->getName(),
						'referenz_name'         => $referenz_name_ref,
						'referenz_name_org'     => str_replace( 'tbl_', '', strtolower( $referenz_name_ref_org ) ),
						'quelle_name'           => $referenz_name_own,
						'quelle_name_org'       => str_replace( 'tbl_', '', strtolower( $referenz_name_own_org ) ),
						'classname'             => ORMBuilderBase::makeClassName( $table->getName() ),
						'quelle_classname'      => ORMBuilderBase::makeClassName( $table->getName() ),
						'quelle_tablename'      => $table->getName(),
						'type'                  => $ziel_type,
						'type_ziel'             => $own_type,
						'col_ref'               => $own_column->getName(),
						'col_ref_attributename' => ORMBuilderBase::formatAttributename( $own_column->getName() ),
						'col_own'               => $ref_column->getName(),
						'col_own_attributename' => ORMBuilderBase::formatAttributename( $ref_column->getName() ),
						'ref_table'             => NULL,
						'assigned'              => TRUE,
						'nullable'              => FALSE,
					];
					if ( $table->getName() == $ref_table->getName() )
						$myArray['nullable'] = $own_column->isNullable();

					$this->fk_tables[ $ref_table->getName() ][] = $myArray;
				}
			}

			// next
		}

		// referenztabellen markieren - die selber keine fks besitzen
		$this->fk_tables['unassigned'] = [];
		if ( $this->config->getReferenceIsUnassigned() == FALSE )
			foreach ( $myTables as $table )
			{
				$assigned = FALSE;
				foreach ( $this->m2m_tables as $quertabelle )
				{
					if ( in_array( $table->getName(), $quertabelle ) )
						$assigned = TRUE;
				}
				if ( count( $table->getForeignKeys() ) == 0 and $assigned == FALSE )
					$this->fk_tables['unassigned'][] = $table->getName();
			}
		//
	}

	private function build()
	{
		$myDatabase = DBAbstractionCreole::getDatabase( $this->connection->getDatabaseInfo() );
		foreach ( $myDatabase->getTables() as $myTable )
		{
			// DAO klassen bauen
			ORMBuilderQueryClass::build( $this->config, $myTable, $this->fk_tables, $this->m2m_tables );
			ORMBuilderDAOClass::build( $this->config, $myTable, $this->fk_tables, $this->m2m_tables );
			ORMBuilderBaseObjectClass::build( $this->config, $myTable, $this->fk_tables );
			ORMBuilderAbstractionClass::build( $this->config, $myTable, $this->fk_tables, $this->m2m_tables );
			ORMBuilderBaseClass::build( $this->config, $myTable, $this->fk_tables, $this->m2m_tables );
			ORMBuilderListClass::build( $this->config, $myTable );

			if ( $this->config->isGenerateJsClasses() and $this->config->getJsPath() )
			{
				ORMBuilderJSDAOClass::build( $this->config, $myTable, $this->fk_tables, $this->m2m_tables );
				ORMBuilderJSBaseObjectClass::build( $this->config, $myTable, $this->fk_tables, $this->m2m_tables );
			}
		}

		if( !$this->config->getIsUseNamespaces() )
			ORMBuilderLibraryClass::build( $this->config, $myDatabase );

		if ( $this->config->getIncludeSystem() )
			ORMBuilderTemplateClass::build( $this->config, $myDatabase );

		echo "Alle Dateien wurden erfolgreich erstellt!" . PHP_EOL;
	}

	private function detectM2M(): bool
	{

	}
}