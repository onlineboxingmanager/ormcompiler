<?php
#ä
abstract class ORMBuilderBase
{

	protected static string $path;
	protected static string $builder_version = '1.0.0 (php8+)';
	protected static array $builder_author = ['Mario Kaufmann'];
	protected static ?ORMConfig $config = null;

	protected static int $chmod_dir = 0777;
	protected static int $chmod_file = 0644;
	protected static string $unix_owner = 'www-data';
	protected static string $unix_group = 'www-data';

	public static function setConfig(ORMConfig $myConfig): void
    {
		self::$config = $myConfig;
	}

	public static function getConfig(): ORMConfig
    {
		return static::class::$config;
	}

	public static function TAB(): string
    {
		return chr(9);
	}

	public static function LN(): string
    {
		return "\n";
	}

	public static function HEADER(): string
    {
		return '<?php'.self::LN().'#ä'.self::LN();
	}

	public static function COMMENT(string $classname = '', string $additional = ''): string
    {
		$content = '/**'.self::LN();
		$content .= ' * '.$classname.self::LN();
		$content .= ' * @author '.implode(', ',self::$builder_author).self::LN();
		$content .= ' * @version '.self::$builder_version.self::LN();
        $content .= $additional;
		$content .= ' */'.self::LN();
        $content .= '#[\AllowDynamicProperties]'.self::LN();
		return $content;
	}

	public static function FOOTER(): string
    {
		return '}'.self::LN();
	}

	public static function isWindows()
	{
		//By default, we assume that PHP is NOT running on windows.
		$isWindows = false;

		//If the first three characters PHP_OS are equal to "WIN",
		//then PHP is running on a Windows operating system.
		if(strcasecmp(substr(PHP_OS, 0, 3), 'WIN') == 0){
			$isWindows = true;
		}

		return $isWindows;
	}

	public static function write(string $path, string $content): false|int
    {
	    $path = str_replace('//', '/', $path);

	    self::createPath($path);

		echo  'write: ' . $path . PHP_EOL;

		return file_put_contents($path, $content);
	}

	protected static function createPath(string $realpath): void
    {
		$path = '';

		$realpath = str_replace('//', '/', $realpath);

		for($i=0; $i<strlen($realpath); $i++)
        {
			$char = substr($realpath, $i, 1);
			$path .= $char;
			if( $char == '/' AND !is_dir($path) )
			{
					@mkdir($path, self::$chmod_dir, true);
			}

			if( $char == '/' AND is_dir($path) )
				if( !self::isWindows() )
					@chmod($path, self::$chmod_dir);
		}
	}

	public static function checkTypes( string $type ): bool
    {
		return ( in_array($type, array('string', 'integer', 'boolean', 'float', 'double')) );
	}

	public static function getPHPType(string $type): string
	{
		switch (strtolower($type)){
			case 'str':
			case 'string':
			case 'varchar':
			case 'char':
			case 'set':
			case 'enum':
			case '':
			case 'text':
			case 'longvarchar':
			case 'longtext':
				return 'string';
			case 'int':
			case 'tinyint':
			case 'integer':
			case 'time':
			case 'timestamp':
			case 'date':
			case 'numeric':
				return 'int';
			case 'bol':
			case 'boolean':
			case 'bool':
				return 'bool';
			case 'double':
			case 'dbl':
			case 'flt':
			case 'float':
			case 'decimal':
				return 'float';
			case 'binary':
				return 'UUID';
			default:
				return $type;
		}
	}

	public static function getTypescriptType(string $type): string
	{
		switch (strtolower($type))
		{
			case 'str':
			case 'string':
			case 'varchar':
			case 'char':
			case 'set':
			case 'enum':
			case '':
			case 'text':
			case 'binary':
				return 'string';

			case 'double':
			case 'dbl':
			case 'flt':
			case 'float':
			case 'decimal':
			case 'int':
			case 'tinyint':
			case 'integer':
			case 'numeric':
				return 'number';

			case 'time':
			case 'timestamp':
			case 'date':
				return 'Date';

			case 'bol':
			case 'boolean':
			case 'bool':
				return 'boolean';

			default:
				return $type;
		}
	}

	public static function formatName(string $string): string
    {
		return ucwords($string);
	}

	public static function formatAttributename(string $colName): string
    {
		$attName = strtolower($colName);
		$firstChar = strtoupper( substr($attName,0,1) );
		$attName = implode('', array_map('ucfirst', explode('_', $attName))); // underscores entfernen und Teilworte gross schreiben
		return substr_replace($attName, $firstChar, 0, 1);
	}

	public static function checkDefaultValue(string $value): bool
    {
		$value = trim($value);
		$check = true;
		if(strlen($value) == 0)
			$check = false;
		if($value == 'CURRENT_TIMESTAMP')
			$check = false;

		return $check;
	}

	public static function makeClassName(string $name, $use_namespace = true): string
    {
		$name = str_replace(array('new_', 'tbl_'), '', strtolower($name));
		$name = self::formatAttributename($name);

		#if( $use_namespace )
		#	$name = '\Enduron\Models\\' . $name . '\\' . $name;

		return $name;
	}

	public static function makeRefName(string $name): string
    {
		$name = str_replace(array('new_', 'tbl_'), '', strtolower($name));
		if(strpos($name, '_') == 0 )
			return $name;
		$pieces = explode('_', $name);
		$name = '';
		for($i=1; $i<count($pieces); $i++)
        {
			$name .= $pieces[$i];
		}
		return $name;
	}

}