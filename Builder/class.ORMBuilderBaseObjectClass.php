u<?php
#ä
require_once 'class.ORMBuilderBase.php';

abstract class ORMBuilderBaseObjectClass extends ORMBuilderBase
{

	public static function build ( ORMConfig $myConfig, DBTable $myTable, array $fk_tables = null )
	{
		self::setConfig( $myConfig );
		$myContent = parent::HEADER();
		//$myContent .= 'Library::requireLibrary(LibraryKeys::SYSTEM_UTILITIES_OBJECT());'.parent::LN();

		if( self::$config->getIsUseNamespaces() )
		{
			$myContent .= 'namespace Enduron\Models\\'.self::makeClassName($myTable->getName()) . ';' . parent::LN();
			$myContent .= 'use Enduron\Core\ORM\BaseObject;' . parent::LN();
			$myContent .= 'use Enduron\Models\\'.self::makeClassName($myTable->getName()).'\\'.self::makeClassName($myTable->getName()).'DAO;' . parent::LN();
			$myContent .= 'use Enduron\Models\\'.self::makeClassName($myTable->getName()).'\\'.self::makeClassName($myTable->getName()).'List;' . parent::LN();
			$myContent .= 'use Enduron\Models\\'.self::makeClassName($myTable->getName()).'\\'.self::makeClassName($myTable->getName()).'Query;' . parent::LN();
			$myContent .= 'use Enduron\Core\DBAL\SQLLimit;' . parent::LN();
			$myContent .= 'use Enduron\Core\Utilities\UUID;' . parent::LN()
				. parent::LN();
		}

		$myContent .= parent::LN();


		$additional = ' * @method static ' . ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\'.self::makeClassName( $myTable->getName() ).'\\' : '' ) . self::makeClassName( $myTable->getName() ) . ' find($value)'.self::LN();
        $additional .= ' * @method static ' . ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\'.self::makeClassName( $myTable->getName() ).'\\' : '' ) . self::makeClassName( $myTable->getName() ) . 'List get(mixed $value = null)'.self::LN();
        $additional .= ' * @method static ' . ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\'.self::makeClassName( $myTable->getName() ).'\\' : '' ) . self::makeClassName( $myTable->getName() ) . 'Query where(string $field, mixed $operator = null, mixed $value = null)'.self::LN();
        $additional .= ' * @method static ' . ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\'.self::makeClassName( $myTable->getName() ).'\\' : '' ) . self::makeClassName( $myTable->getName() ) . 'Query all()'.self::LN();

		$myContent .= parent::COMMENT( self::makeClassName( $myTable->getName() ) . ' Base-Object', $additional );
		$myContent .= 'abstract class ' . self::makeClassName( $myTable->getName() ) . 'Base extends BaseObject' . parent::LN();
        $myContent .= '{'.parent::LN(). parent::LN();
		$myContent .= self::buildAttributes( $myTable, $fk_tables );
		$myContent .= self::buildMethods( $myTable, $fk_tables );
		$myContent .= parent::FOOTER();

		$filename = ( self::$config->getIsUseNamespaces() ? '' : 'class.' ) . self::makeClassName( $myTable->getName() ) . 'Base.php';
		$path = $myConfig->getApplicationPath();
		if ( substr( $path, -1 ) == '/' )
			$path = substr( $path, 0, -1 );
		$myName = str_replace( array( 'new_', 'tbl_' ), '', strtolower( $myTable->getName() ) );

		if( self::$config->getIsUseNamespaces() )
			$myName = str_replace('_', '', ucwords($myName, '_'));

		$myFolder = explode( '_', $myName );
		foreach ( $myFolder as $folder )
		{
			$path .= '/' . parent::formatName( $folder );
		}
		$path_folder = $path;
		$path = $path . '/' . $filename;

		self::write( $path, $myContent );

		$object_path = $path_folder . '/class.' . self::makeClassName( $myTable->getName() ) . '.php';
		if ( !file_exists( $object_path ) )
			ORMBuilderObjectClass::build( $myConfig, $myTable );
	}

	private static function buildAttributes ( DBTable $myTable, array $fk_tables = null )
	{
		$content = parent::TAB() . "// references" . parent::LN();

		// new method
		$fk_columns = array();
		$ref_list = [];
		if ( isset( $fk_tables[ $myTable->getName() ] ) and !in_array( $myTable->getName(), $fk_tables['unassigned'] ) )
		{
			foreach ( $fk_tables[ $myTable->getName() ] as $reference )
			{
				$classname = $reference['table'];
				$classname = self::makeClassName($classname);

				$is_nullable = ( $reference['nullable'] == true ? ' = NULL' : null );

				$referencetype = '';
				if ( $reference['type'] == DBReferenceTypes::REFERENCE_MANY_TO_MANY or $reference['type'] == DBReferenceTypes::REFERENCE_SINGLE_TO_MANY )
				{
					$referencetype = '_list';
					$classname .= 'List';
				}

				$namespace = ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\' . self::makeClassName($reference['table']) . '\\' : '' );

				$classname = $namespace . $classname;

				if( $is_nullable )
					$content .= parent::TAB() . 'protected ?' . $classname . ' $ref_' . $reference['referenz_name_org'] . $referencetype . ' = null;';
				else
					$content .= parent::TAB() . 'protected ' . $classname . ' $ref_' . $reference['referenz_name_org'] . $referencetype . ';';


				//$content .= ' // ' . $reference['ref_table'];
				$content .= PHP_EOL;

				$fk_columns[] = self::makeRefName( $reference['col_own'] );

				$ref_list[ $reference['col_own'] ] = $reference['referenz_name_org'];
			}
		}

		$content .= parent::LN();
		$content .= parent::TAB() . "// attributes" . parent::LN();
		foreach ( $myTable->getColumns() as $column )
		{
			/** @var DBColumn $column */

			//@TODO: funzt noch net
			// skip "single-to-single" reference - [Nur Objektzugriff erlaubt]
			//if( in_array($column->getName(), $fk_columns) AND $reference['type'] == DBReferenceTypes::REFERENCE_SINGLE_TO_SINGLE )
			//continue;

			if( in_array($column->getName(), ['userid_created', 'userid_modified', 'userid_deleted', 'tstamp_created', 'tstamp_modified', 'tstamp_deleted']) )
				continue;

			$typename = parent::getPHPType( strtolower( DBTypes::getCreoleName( $column->getType() ) ) );

			$defaultvalue = ( (is_numeric($column->getDefaultvalue()) OR $typename == 'int' ) ? (int) $column->getDefaultvalue() : '\'' . $column->getDefaultvalue() . '\'' );
			if( $typename == 'float' )
				$defaultvalue = (float) $column->getDefaultvalue();

			if( $typename == 'UUID' )
				$content .= parent::TAB() . 'private ?\\Enduron\\Core\\Utilities\\' . $typename . ' $' . strtolower( $column->getName() ) .  ' = null;' . parent::LN();
			elseif( $column->getIsNullable() )
				$content .= parent::TAB() . 'private ?' . $typename . ' $' . strtolower( $column->getName() ) .  ' = null;' . parent::LN();
			else
				$content .= parent::TAB() . 'private ' . $typename . ' $' . strtolower( $column->getName() ) .  ' = ' . $defaultvalue . ';' . parent::LN();
		}
		$content .= parent::LN();

		//$content .= parent::TAB() . 'protected ' . '$tablename = \'' . $myTable->getName() . '\';' . parent::LN();

		// primary key
		#echo "<hr>";
		#var_dump( $myTable->getName() );
		#var_dump( $myTable->getPrimaryKey()->getName() );
		#echo "<hr>";

		// attributes
		/*
		$content .= parent::TAB() . "protected \$properties = [" . parent::LN();
		foreach ( $myTable->getColumns() as $column )
		{

			if ( !$column->getType() )
			{
				echo "Typ nicht gefunden";
				var_dump( $column );
				die();
			}

			$content .= parent::TAB() . parent::TAB() . '\'' . strtolower( $column->getName() ) . '\' => [' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'size\' => ' . $column->getSize() . ',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'type\' => ' . $column->getType() . ',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'typename\' => \'' . $column->getTypname() . '\',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'default\' => ' . ( $column->getDefaultvalue() ? '\'' . $column->getDefaultvalue() . '\'' : 'NULL' ) . ',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'is_autoincrement\' => ' . ( $column->getIsAutoIncrement() ? 'true' : 'false' ) . ',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'is_nullable\' => ' . ( $column->getIsNullable() ? 'true' : 'false' ) . ',' . parent::LN();

			// is pk
			if ( $myTable->getPrimaryKey()->getName() == $column->getName() )
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'is_primarykey\' => true,' . parent::LN();
			else
			{
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'is_primarykey\' => false,' . parent::LN();
			}

			// references
			if ( $myTable->getPrimaryKey()->getName() == $column->getName() )
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'reference\' => null,' . parent::LN();
			else
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'reference\' => ' . ( isset( $ref_list[ $column->getName() ] ) ? '\'' . $ref_list[ $column->getName() ] . '\'' : 'null' ) . ',' . parent::LN();

			$content .= parent::TAB() . parent::TAB() . '],' . parent::LN();
		}
		$content .= parent::TAB() . "];" . parent::LN();
		*/

		return $content;
	}

	private static function buildMethods ( DBTable $myTable, array $fk_tables = null )
	{
		// foreignkeys - references
		$content = parent::TAB() . '/**************************** REFERENCES ****************************/' . parent::LN();

		// new method
		$fk_columns = array();
		if ( isset( $fk_tables[ $myTable->getName() ] ) and !in_array( $myTable->getName(), $fk_tables['unassigned'] ) )
			foreach ( $fk_tables[ $myTable->getName() ] as $reference )
			{
				$attributename = $reference['referenz_name_org'];

				//$reference_name = $reference['referenz_name'];
				$reference_name = $reference['referenz_name_org'];

				$classname = $reference['classname'];
				$namespace = ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\' . $classname . '\\' : '' );

				$fk_columns[] = $attributename;
				$isMany = false;
				if ( $reference['type'] == DBReferenceTypes::REFERENCE_MANY_TO_MANY or $reference['type'] == DBReferenceTypes::REFERENCE_SINGLE_TO_MANY )
				{
					$attributename .= '_list';
					$classname .= 'List';
					$isMany = true;
				}
				$is_nullable = ( $reference['nullable'] == true ? ' = NULL' : null );
				$nullable = '';

				$content .= parent::TAB() . 'public function ' . self::makeAttSetFunction( $reference_name ) . ( $isMany ? 'List' : '' ) . '(' . ( $is_nullable ? '?' : '' ) . $namespace . $classname . ' $myObject' . $nullable . '): void' . parent::LN();
				$content .= parent::TAB() . '{'. parent::LN();
				$content .= parent::TAB() . parent::TAB() . '$this->ref_' . strtolower( $attributename ) . ' = $myObject;' . parent::LN();
				if ( $isMany != true )
				{
					if ( $is_nullable != null )
						$content .= parent::TAB() . parent::TAB() . 'if(!is_null($myObject))' . parent::LN();
					$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$this->set' . $reference['col_own_attributename'] . '($myObject->get' . $reference['col_ref_attributename'] . '());' . parent::LN();
				}
				$content .= parent::TAB() . parent::TAB() . '$this->_setIsLoaded(\'ref_' . strtolower( $attributename ) . '\');' . parent::LN();
				$content .= parent::TAB() . '}' . parent::LN() . parent::LN();

				$content .= parent::TAB() . 'public function ' . self::makeAttGetFunction( $reference_name, $isMany )  . ': ' . ($is_nullable ? '?' : '') . $namespace . $classname . parent::LN();
				$content .= parent::TAB() . '{'. parent::LN();
				$content .= parent::TAB() . parent::TAB() . 'if( !$this->_getIsLoaded(\'ref_' . strtolower( $attributename ) . '\') '.($is_nullable ? ' AND $this->' . $reference['col_own'] : '').')' . parent::LN();
				#$content .= parent::TAB().parent::TAB().parent::TAB().'$this->ref_'.strtolower($attributename).' = '.self::makeClassName($myTable->getName()).'Manager::get'.$reference_name.($isMany ? 'List' : '').'By'.self::makeClassName($myTable->getName()).'($this'.($isMany ? ', $myLimit' : '').');'.parent::LN();

				$namespace = ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\' . self::makeClassName( $myTable->getName() ) . '\\' : '' );

				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$this->' . self::makeAttSetFunction( $reference_name ) . ( $isMany ? 'List' : '' ) . '('. $namespace . self::makeClassName( $myTable->getName() ) . 'Manager::get' . self::makeClassName( $reference_name ) . ( $isMany ? 'List' : '' ) . 'By' . self::makeClassName( $myTable->getName() ) . '($this' . ( $isMany ? ', $myLimit' : '' ) . '));' . parent::LN();
				$content .= parent::TAB() . parent::TAB() . 'return $this->ref_' . strtolower( $attributename ) . ';' . parent::LN();
				$content .= parent::TAB() . '}' . parent::LN() .
					parent::LN();
			}

		// attribute setter and getter
		$content .= parent::TAB() . '/**************************** ATTRIBUTES ****************************/' . parent::LN();
		foreach ( $myTable->getColumns() as $column )
		{
			// skip "single-to-single" reference - [Nur Objektzugriff erlaubt]
			if ( in_array( $column->getName(), $fk_columns ) OR in_array($column->getName(), ['userid_created', 'userid_modified', 'userid_deleted', 'tstamp_created', 'tstamp_modified', 'tstamp_deleted']) )
				continue;

			$typename = parent::getPHPType( strtolower( DBTypes::getCreoleName( $column->getType() ) ) );

			$null_check = 'is_null($' . $typename . ')'; // default null check
			$attr_check = 'is_' . self::getPHPType( $typename ) . '($' . $typename . ')' . ( $column->getIsNullable() == true ? ' OR is_null($' . $typename . ')' : null ) . '';
			/*
			switch ( self::getPHPType($typename) ) {
				case 'string':
					$null_check = '!isset($'.$column->getType().') OR strlen($'.$column->getType().') == 0';
					$attr_check = 'is_string($'.$column->getType().')'.( $attribute['nn'] == FALSE ? ' OR is_null($'.$attribute['type'].')' : null ).'';
					break;
				case 'integer':
					if( $attribute['pk'] == true OR $attribute['fk'] == true)
						$null_check = 'empty($'.$column->getType().')';
					$attr_check = 'is_integer($'.$column->getType().')'.( $attribute['nn'] == FALSE ? ' OR is_null($'.$column->getType().')' : null ).'';
					break;
				case 'float':
					$attr_check = 'is_float($'.$column->getType().')'.( $attribute['nn'] == FALSE ? ' OR is_null($'.$column->getType().')' : null ).'';
					break;
				case 'double':
					$attr_check = 'is_double($'.$column->getType().')'.( $attribute['nn'] == FALSE ? ' OR is_null($'.$column->getType().')' : null ).'';
					break;
				case 'date':
				case 'time':
					$column->getType() = 'integer';
					$null_check = 'empty($'.$column->getType().')';
					$attr_check = 'is_integer($'.$column->getType().')'.( $attribute['nn'] == FALSE ? ' OR is_null($'.$column->getType().')' : null ).'';
					break;
			}
			*/

			$phptype = self::getPHPType( $typename );
			if( $phptype == 'UUID' )
				$phptype = '\\Enduron\\Core\\Utilities\\UUID';

			// setter
			$content .= parent::TAB() . 'public function ' . self::makeAttSetFunction( $column->getName() ) . '( ' . ( $column->getIsNullable() ? '?' : '' ) . $phptype . ' $' . self::getPHPType( $typename ) . '): void' . parent::LN();
			$content .= parent::TAB() . '{' . parent::LN();
			/*
			if ( $column->getIsNullable() == false )
			{
				$content .= parent::TAB() . parent::TAB() . 'if(' . $null_check . ')' . "\r\n";
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'throw new ExceptionObject(ExceptionKeys::ERROR_VALUE_NULL, array(\'Attribute: ' . $column->getName() . '\'));' . parent::LN();
			}
			$content .= parent::TAB() . parent::TAB() . 'if(' . $attr_check . '){' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'if( $this->' . strtolower( $column->getName() ) . ' !== $' . $typename . ' ){' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . parent::TAB() . '$this->' . strtolower( $column->getName() ) . ' = $' . $typename . ';' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . parent::TAB() . '$this->_setModified(\'' . strtolower( $column->getName() ) . '\');' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '}' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . '}else{' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'throw new ExceptionObject(ExceptionKeys::ERROR_VALUE_WRONGDATATYPE,array(\'Attribute: ' . $column->getName() . ' | Type: ' . $typename . '\',$' . $typename . '));' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . '}' . parent::LN();
			*/

			$content .= parent::TAB() . parent::TAB() . 'if( $this->' . strtolower( $column->getName() ) . ' !== $' . $typename . ' )' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . '{'.parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$this->' . strtolower( $column->getName() ) . ' = $' . $typename . ';' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$this->_setModified(\'' . strtolower( $column->getName() ) . '\');' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . '}'.parent::LN();

			$content .= parent::TAB() . '}' . parent::LN();
			$content .= parent::LN();

			// getter
			$content .= parent::TAB() . 'public function ' . self::makeAttGetFunction( $column->getName() ) . ': ' . ( $column->getIsNullable() ? '?' : '' ) . $phptype . parent::LN();
			$content .= parent::TAB() . '{' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . 'return $this->' . strtolower( $column->getName() ) . ';' . parent::LN();
			$content .= parent::TAB() . '}' . parent::LN();
			$content .= parent::LN();
		}
		return $content;
	}

	private static function makeAttSetFunction ( $attname )
	{
		$attname = 'set' . parent::formatAttributename( strtolower( $attname ) );
		return $attname;
	}

	private static function makeAttGetFunction ( $attname, $isMany = false )
	{
		$attname = 'get' . parent::formatAttributename( strtolower( $attname ) ) . ( $isMany == true ? 'List(SQLLimit $myLimit = null)' : '()' );
		return $attname;
	}

}