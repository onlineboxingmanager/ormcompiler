<?php
#ä
require_once 'class.ORMBuilderBase.php';

abstract class ORMBuilderDAOClass extends ORMBuilderBase
{

	/*
	public static function build(ORMConfig $myConfig, TableInfo $myTable){
		self::setConfig($myConfig);
		$driver = self::makeClassName($myConfig->getDbDriver());
		$driver = 'Oracle';
		$class = 'ORMBuilderDAOCreoleClass';
		require_once ORMCOMPILER_PATH.'/Builder/DAO/'.strtolower($driver).'/class.ORMBuilderDAOCreoleClass.php';
		$myDAOBuilder = new $class(DBConnection::getConnection($myConfig));
		$myContent = $myDAOBuilder->run($myTable);

		$filename = 'class.'.self::makeClassName($myTable->getName()).'DAO.php';
		$path = $myConfig->getSystemPath().'Database/DAO/'.$filename;
		self::write($path,$myContent);
	}
	*/

	public static function build ( ORMConfig $myConfig, DBTable $myTable, array $fk_tables, array $m2m_tables ): void
	{
		self::setConfig( $myConfig );
		$myContent = parent::HEADER();

		if( self::$config->getIsUseNamespaces() )
		{
			$myContent .= 'namespace Enduron\Models\\'.self::makeClassName($myTable->getName()).';' . parent::LN();
			$myContent .= 'use Enduron\Core\ORM\BaseDAO;' . parent::LN();
			$myContent .= 'use Enduron\Core\DBAL\DBResult;' . parent::LN();
			$myContent .= 'use Enduron\Core\DBAL\SQLLimit;' . parent::LN();
			$myContent .= 'use Enduron\Core\Utilities\UUID;' . parent::LN();
			$myContent .= parent::LN();
		}
		else
		{
			$myContent .= 'Library::requireLibrary(LibraryKeys::ABSTRACTION_DAO_BASE());' . parent::LN();
		}

		$myContent .= parent::COMMENT( self::makeClassName( $myTable->getName() ) . ' DAO-Class [DAO]' );
		$myContent .= 'abstract class ' . self::makeClassName( $myTable->getName() ) . 'DAO extends BaseDAO' . parent::LN();
        $myContent .= '{' . parent::LN().parent::LN();
		$myContent .= parent::TAB() . 'public const TABLENAME = \'' . strtolower( $myTable->getName() ) . '\';' . parent::LN() . parent::LN();
		$myContent .= self::buildPrimaryKeys( $myTable );
		$myContent .= self::buildColumns( $myTable, $fk_tables );
		$myContent .= self::buildQuery( $myTable, $fk_tables );
		$myContent .= self::buildFetchFromResult( $myTable, $fk_tables );
		//$myContent .= self::buildStore( $myTable, $fk_tables );
		#$myContent .= self::buildInsert( $myTable, $fk_tables );
		#$myContent .= self::buildUpdate( $myTable, $fk_tables );
		$myContent .= parent::FOOTER();

		$filename = ( self::$config->getIsUseNamespaces() ? '' : 'class.' ) . self::makeClassName( $myTable->getName() ) . 'DAO.php';

		$path = $myConfig->getSystemPath() . 'Database/DAO/' . $filename;

		if( self::$config->getIsUseNamespaces() )
		{
			$path = $myConfig->getApplicationPath();
			if( substr($path, -1) == '/' )
				$path = substr($path, 0, -1);
			$myName = str_replace(array('new_', 'tbl_'), '', strtolower($myTable->getName()));
			$myName = str_replace('_', '', ucwords($myName, '_'));
			$path .= '/'.parent::formatName($myName);
			$path = $path.'/'.$filename;
		}

		self::write( $path, $myContent );
	}

	private static function buildPrimaryKeys ( DBTable $myTable )
	{
        $content = parent::TAB() . 'protected static array $primarykeys = [' . parent::LN();
		$pk = array();
		foreach ( $myTable->getPrimaryKey()->getColumns() as $column )
		{
			$pk[] = parent::TAB() . parent::TAB() . '\'' . $column->getName() . '\' => \'get' . self::formatAttributename( $column->getName() ) . '\'';
		}
		$content .= implode( ',' . parent::LN(), $pk ) . parent::LN();
		$content .= parent::TAB() . '];' . parent::LN();
		$content .= self::LN();
        /*
		$content .= parent::TAB() . 'public static function getPrimaryKeys(): array' . parent::LN();
		$content .= parent::TAB() . '{' . parent::LN();
		$content .= parent::TAB() . parent::TAB() . 'return self::$primarykeys;' . self::LN();
		$content .= parent::TAB() . '}' . self::LN();
		$content .= self::LN();
        */
		return $content;
	}

	private static function buildColumns ( DBTable $myTable, array $fk_tables )
	{
		$content = '';

		$content .= parent::TAB() . "protected static array \$properties = [" . parent::LN();
		foreach ( $myTable->getColumns() as $column )
		{
            /**
             * @var DBColumn $column
             */

			if ( !$column->getType() )
			{
				throw new Exception(" Typ nicht gefunden für spalte ". $column->getName() . " - tabelle: " . $myTable->getName());
			}

			$content .= parent::TAB() . parent::TAB() . '\'' . strtolower( $column->getName() ) . '\' => [' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'size\' => ' . $column->getSize() . ',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'type\' => ' . $column->getType() . ',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'typename\' => \'' . $column->getTypname() . '\',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'default\' => ' . ( !is_null($column->getDefaultvalue()) ? '\'' . $column->getDefaultvalue() . '\'' : 'NULL' ) . ',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'is_autoincrement\' => ' . ( $column->getIsAutoIncrement() ? 'true' : 'false' ) . ',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'is_nullable\' => ' . ( $column->getIsNullable() ? 'true' : 'false' ) . ',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'comment\' => \'' . $column->getComment() . '\',' . PHP_EOL;

			// is pk
			if ( $myTable->getPrimaryKey()->getName() == $column->getName() )
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'is_primarykey\' => true,' . parent::LN();
			else
			{
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'is_primarykey\' => false,' . parent::LN();
			}

            $reference_attribute = 'null';

            // if reference
            if ( isset( $fk_tables[ $myTable->getName() ] ) AND $myTable->getPrimaryKey()->getName() != $column->getName() )
            {
                foreach ( $fk_tables[$myTable->getName()] as $reference )
                {
                    if( isset($reference['col_ref']) AND $reference['col_ref'] === $column->getName() )
                    {
                        $reference_attribute = '\''.$reference['referenz_name_org'].'\'';
                    }
                }
            }

            $content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'reference\' => '.$reference_attribute.',' . parent::LN();
			$content .= parent::TAB() . parent::TAB() . '],' . parent::LN();
		}
		$content .= parent::TAB() . "];" . parent::LN();

        $content .= self::LN();
		$content .= parent::TAB() . 'protected static array $columns = [' . parent::LN();
		$classname = parent::makeClassName( $myTable->getName() );
		$tablename = strtolower( $myTable->getName() );
		$columns = array();
		foreach ( $myTable->getColumns() as $col )
		{
			$col instanceof DBColumn;
			// hier muss der manager vorher required sein
			//$col_content = parent::TAB().parent::TAB().$classname.'Manager::'.strtoupper($col->getName()). ' => \'';
			// so isses erstmal performanter
			$col_content = parent::TAB() . parent::TAB() . '\'' . $col->getName() . '\' => \'';
			if ( $col->getType() == DBTypes::TIMESTAMP or $col->getType() == DBTypes::DATE )
				$col_content .= 'UNIX_TIMESTAMP(' . $tablename . '.' . $col->getName() . ') as ' . strtoupper( self::makeClassName( $tablename ) ) . '_' . strtoupper( $col->getName() );
			elseif ( $col->getType() == DBTypes::BINARY )
				$col_content .= 'BIN_TO_UUID(' . $tablename . '.' . $col->getName() . ') as ' . strtoupper( self::makeClassName( $tablename ) ) . '_' . strtoupper( $col->getName() );
			else
				$col_content .= $tablename . '.' . $col->getName() . ' as ' . strtoupper( self::makeClassName( $tablename ) ) . '_' . strtoupper( $col->getName() );
			$col_content .= '\'';
			$columns[] = $col_content;
		}
		$content .= implode( ',' . parent::LN(), $columns ) . parent::LN();
		$content .= parent::TAB() . '];' . parent::LN();
		$content .= self::LN();

        /*
		$content .= parent::TAB() . 'public static function getColumns(): array' . parent::LN();
        $content .= parent::TAB() . '{' . parent::LN();
		$content .= parent::TAB() . parent::TAB() . 'return self::$columns;' . self::LN();
		$content .= parent::TAB() . '}' . self::LN();
		$content .= self::LN();

		$content .= parent::TAB() . 'public static function getProperties(): array' . parent::LN();
        $content .= parent::TAB() . '{' . parent::LN();
		$content .= parent::TAB() . parent::TAB() . 'return self::$properties;' . self::LN();
		$content .= parent::TAB() . '}' . self::LN();
		$content .= self::LN();

        */

		return $content;
	}

	private static function buildQuery ( DBTable $myTable, array $fk_tables )
	{
		$content = parent::TAB() . 'public static function get' . self::makeClassName( $myTable->getName() ) . 'ListByQuery(string $where, array $params = null, SQLLimit $myLimit = null): ' . ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\' . self::makeClassName( $myTable->getName() ) . '\\' : '') . self::makeClassName( $myTable->getName() ) . 'List' . self::LN();
        $content .= parent::TAB() . '{' . parent::LN();
		$content .= parent::TAB() . parent::TAB() . '$myResult = self::genericQuery(\'' . self::LN();
		$content .= parent::TAB() . parent::TAB() . 'SELECT' . self::LN();

		$content .= parent::TAB() . parent::TAB() . parent::TAB() . "'.implode(\", \\n\", self::\$columns).'" . self::LN();
		$content .= parent::TAB() . parent::TAB() . 'FROM' . self::LN();
		$content .= parent::TAB() . parent::TAB() . parent::TAB() . '\'.self::TABLENAME.\'' . self::LN();
		$content .= parent::TAB() . parent::TAB() . 'WHERE \'.$where, $params, $myLimit);' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$myList = new ' . ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\' . self::makeClassName( $myTable->getName() ) . '\\' : '') . self::makeClassName( $myTable->getName() ) . 'List();' . self::LN();
		$content .= parent::TAB() . parent::TAB() . 'while($myResult->next())' . self::LN();
		$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$myList->add(self::get' . self::makeClassName( $myTable->getName() ) . 'FromResult($myResult));' . self::LN();
		$content .= parent::TAB() . parent::TAB() . 'return $myList;' . self::LN();
		$content .= parent::TAB() . '}' . self::LN();
		$content .= self::LN();
		return $content;
	}

	private static function buildFetchFromResult ( DBTable $myTable, $fk_tables )
	{
		#$content = parent::TAB() . '/**' . parent::LN();
		#$content .= parent::TAB() . ' * @param ResultSet $myDataObject' . parent::LN();
		#$content .= parent::TAB() . ' * @return ' . self::makeClassName( $myTable->getName() ) . '' . parent::LN();
		#$content .= parent::TAB() . ' */' . parent::LN();

		$content = parent::TAB() . 'public static function get' . self::makeClassName( $myTable->getName() ) . 'FromResult(DBResult $myResult, bool $load_references_from_database = false, string $customname = \'\'): ' . ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\' . self::makeClassName( $myTable->getName() ) . '\\' : '') . self::makeClassName( $myTable->getName() ) . '|null' . self::LN();
        $content .= parent::TAB() . '{' . self::LN();

        $content .= parent::TAB() . parent::TAB() . '$row = $myResult->getRow();' . self::LN();
        $content .= self::LN();

		$content .= parent::TAB() . parent::TAB() . '$pk = array_keys(self::$primarykeys)[0];' . self::LN();
		$content .= parent::TAB() . parent::TAB() . 'if( !empty($pk) AND !isset($row[strtoupper($customname.\'' . self::makeClassName( $myTable->getName() ) . '_\'.$pk)]) )' . self::LN();
		$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'return null;' . self::LN();
        $content .= self::LN();

		/*

		$content .= parent::TAB().parent::TAB().'// return null on empty left joins'.self::LN();
		foreach ($myTable->getPrimaryKey()->getColumns() as $column){
			$column instanceof DBColumn;
			$colname = strtoupper(self::makeClassName($myTable->getName()).'_'.strtoupper($column->getName()));
			$content .= parent::TAB().parent::TAB().'if( !$myResult->get($customname.\''.$colname.'\') OR $myResult->get($customname.\''.$colname.'\') == \'NULL\' )'.self::LN();
			$content .= parent::TAB().parent::TAB().parent::TAB().'return null;'.self::LN();
		}
		*/

        $content .= parent::TAB() . parent::TAB() . '// get attributes from result "tablename + columnname"' . self::LN();

		if( !self::$config->getIsUseNamespaces() )
			$content .= parent::TAB() . parent::TAB() . 'Library::requireLibrary(LibraryKeys::APPLICATION_' . str_replace( 'TBL_', '', strtoupper( $myTable->getName() ) ) . '());' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$myObject = new ' . ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\' . self::makeClassName( $myTable->getName() ) . '\\' : '') . self::makeClassName( $myTable->getName() ) . '();' . self::LN();

		// cols
		foreach ( $myTable->getColumns() as $col )
		{
			$type = DBCreoleTypes::getCreoleFunctionNames( self::getPHPType( $col->getTypname() ) );
			$colname = strtoupper( self::makeClassName( $myTable->getName() ) . '_' . $col->getName() );
            $is_nullable = $col->getIsNullable();

            /*
            if( $is_nullable )
                $content .= parent::TAB() . parent::TAB() . 'if( !is_null($myResult->get' . self::formatAttributename( $type ) . '($customname.\'' . $colname . '\')) )' . self::LN() . parent::TAB();
            */
			$content .= parent::TAB() . parent::TAB() . '$myObject->setAttribute' . '(\''.$col->getName().'\', $myResult->get' . self::formatAttributename( $type ) . '($customname.\'' . $colname . '\'));' . self::LN();
		}

		// references
        $content .= self::LN();
		$content .= parent::TAB() . parent::TAB() . '// references' . self::LN();
		#$content .= parent::TAB().parent::TAB().'if( $load_references_from_database ){'.self::LN();
		if ( isset( $fk_tables[ $myTable->getName() ] ) )
			foreach ( $fk_tables[ $myTable->getName() ] as $reference )
			{
				if ( $reference['type'] != DBReferenceTypes::REFERENCE_SINGLE_TO_SINGLE )
					continue;

				$attributename = $reference['referenz_name_org'];
				$classname = $reference['classname'];
				$isMany = false;
				if ( $reference['type'] == DBReferenceTypes::REFERENCE_MANY_TO_MANY or $reference['type'] == DBReferenceTypes::REFERENCE_SINGLE_TO_MANY )
				{
					$attributename .= '_list';
					$classname .= 'List';
					$isMany = true;
				}

				// selbst referenz
				if ( $reference['table'] == $myTable->getName() )
				{
					// ...
				}
				else
				{
					#if( strtoupper($myTable->getName()) == 'TBL_MANAGER' OR strtoupper($myTable->getName()) == 'TBL_MANAGER_MESSAGE')
					#	var_dump($reference);
					// ## hier abfragen ob existiert
					// TODO: wen der result auch die spalten der referenzen enthält - sollte das Referenz object auch geladen werden
					// strtoupper($reference['referenz_name'].'_'.$reference['col_ref_attributename']);
					//$content .= parent::TAB().parent::TAB().'Library::requireLibrary(LibraryKeys::APPLICATION_'.str_replace('TBL_', '', strtoupper($reference['table'])).'());'.self::LN();
					$content .= parent::TAB() . parent::TAB() . 'if( !empty($row[\'' . strtoupper( $reference['classname'] . '_' . $reference['col_ref_attributename'] ) . '\']) AND $load_references_from_database ){' . self::LN();

					$namespace = ( self::$config->getIsUseNamespaces() ? '\Enduron\Models\\'.$reference['classname'].'\\' : '' );

					if( !self::$config->getIsUseNamespaces() )
						$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'include_once \'class.' . $reference['classname'] . 'DAO.php\';' . self::LN();

					$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$myRef = ' . $namespace . $reference['classname'] . 'DAO::get' . $reference['classname'] . 'FromResult($myResult); // try to build ref-object' . self::LN();
					$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'if( $myRef )' . self::LN();
					$content .= parent::TAB() . parent::TAB() . parent::TAB() . parent::TAB() . '$myObject->set' . self::makeClassName( $reference['referenz_name_org'] ) . '(' . $namespace . $reference['classname'] . 'DAO::get' . $reference['classname'] . 'FromResult($myResult));' . self::LN();
					$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$myObject->_setIsLoaded(\'ref_' . strtolower( $attributename ) . '\');' . self::LN();
					$content .= parent::TAB() . parent::TAB() . '}' . self::LN();
				}

				// OOP method
				//$content .= parent::TAB().parent::TAB().'$myObject->set'.$reference['referenz_name'].'('.$reference['classname'].'Manager::get'.$reference['classname'].'By'.$reference['quelle_name'].'($myObject));'.self::LN();
				// ... hier direkt object übergeben
				#$content .= parent::TAB().parent::TAB().parent::TAB().'$myObject->set'.$reference['referenz_name'].'($myObject->get'.$reference['referenz_name'].'());'.self::LN();
			}
		#$content .= parent::TAB().parent::TAB().'}'.self::LN();

		$content .= parent::TAB() . parent::TAB() . '// set status' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$myObject->_clearModifies(); // clear modified columns' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$myObject->_setIsNew(false); // set status to "existing"' . self::LN();
		$content .= parent::TAB() . parent::TAB() . 'return $myObject;' . self::LN();
		$content .= parent::TAB() . '}' . self::LN();

		$content .= self::LN();
		return $content;
	}

	private static function buildInsert ( DBTable $myTable, $fk_tables )
	{
		$content = parent::TAB() . 'private static function insert(' . self::makeClassName( $myTable->getName() ) . ' $myObject): int' . self::LN();
        $content .= parent::TAB() . '{' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$insert = [];' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$para = [];' . self::LN();
		$content .= self::LN();
		// cols
		foreach ( $myTable->getColumns() as $col )
		{
			$col instanceof DBColumn;
			$value = '?';

			if ( $col->getType() == DBTypes::TIMESTAMP or preg_match( '/^tstamp\_/ui', $col->getName() ) )
			{
				$content .= parent::TAB() . parent::TAB() . 'if( !is_null($myObject->get' . self::formatAttributename( $col->getName() ) . '()) ){' . self::LN();
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$para[] = \'FROM_UNIXTIME(?)\';' . self::LN();
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$insert[] = \'' . $col->getName() . '\';' . self::LN();
				$content .= parent::TAB() . parent::TAB() . '}' . self::LN();
			}

			elseif ( $col->getType() == DBTypes::DATE )
			{
				$content .= parent::TAB() . parent::TAB() . 'if( !is_null($myObject->get' . self::formatAttributename( $col->getName() ) . '()) ){' . self::LN();
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$para[] = \'FROM_UNIXTIME(?)\';' . self::LN();
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$insert[] = \'' . $col->getName() . '\';' . self::LN();
				$content .= parent::TAB() . parent::TAB() . '}' . self::LN();
			}

			elseif ( $col->getType() == DBTypes::BINARY )
			{
				$content .= parent::TAB() . parent::TAB() . 'if( !is_null($myObject->get' . self::formatAttributename( $col->getName() ) . '()) ){' . self::LN();
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$para[] = \'UUID_TO_BIN(?)\';' . self::LN();
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$insert[] = \'' . $col->getName() . '\';' . self::LN();
				$content .= parent::TAB() . parent::TAB() . '}' . self::LN();
			}

			else
			{
				$content .= parent::TAB() . parent::TAB() . '$para[] = \'' . $value . '\';' . self::LN();
				$content .= parent::TAB() . parent::TAB() . '$insert[] = \'' . $col->getName() . '\';' . self::LN();
			}

		}
		$content .= self::LN();
		$content .= parent::TAB() . parent::TAB() . '$conn = DBConnection::getConnection();' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$stmt = $conn->prepareStatement(\'' . self::LN();
		$content .= parent::TAB() . parent::TAB() . 'INSERT INTO \'.self::TABLENAME.\' (\' . implode(\', \', $insert) . \')' . self::LN();
		$content .= parent::TAB() . parent::TAB() . 'VALUES ( \' . implode(\', \', $para) . \')\');' . self::LN();
		$content .= self::LN();
		$content .= parent::TAB() . parent::TAB() . '$paramCount = 1;' . self::LN();
		foreach ( $myTable->getColumns() as $col )
		{
			$col instanceof DBColumn;
			$type = DBCreoleTypes::getCreoleFunctionNames( self::getPHPType( $col->getTypname() ) );
			if ( in_array( $col->getType(), [ DBTypes::TIMESTAMP, DBTypes::DATE ] ) or preg_match( '/^tstamp\_/ui', $col->getName() ) )
			{
				$content .= parent::TAB() . parent::TAB() . 'if( !is_null($myObject->get' . self::formatAttributename( $col->getName() ) . '()) ){' . self::LN();
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$stmt->set' . self::formatAttributename( $type ) . '($paramCount++, $myObject->get' . self::formatAttributename( $col->getName() ) . '());' . self::LN();
				$content .= parent::TAB() . parent::TAB() . '}' . self::LN();
			}
			else
			{
				$content .= parent::TAB() . parent::TAB() . '$stmt->set' . self::formatAttributename( $type ) . '($paramCount++, $myObject->get' . self::formatAttributename( $col->getName() ) . '());' . self::LN();
			}
		}
		$content .= self::LN();

		$myPrimaryKeys = $myTable->getPrimaryKey()->getColumns();

		$content .= parent::TAB() . parent::TAB() . '// execute and get last inserted id' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$stmt->executeUpdate();' . self::LN();
		// default value

		$content .= parent::TAB() . parent::TAB() . '$id = $myObject->get' . self::formatAttributename( $myPrimaryKeys[0]->getName() ) . '();' . self::LN();
		// only if value is integer

		if ( count( $myPrimaryKeys ) == 1 and $myPrimaryKeys[0]->getisAutoincrement() == true )
		{
            $content .= parent::TAB() . parent::TAB() . '$idgen = $conn->lastInsertId();' . self::LN(); // PDO

            /*
            $content .= parent::TAB() . parent::TAB() . '$idgen = $conn->getIdGenerator();' . self::LN();
			$content .= parent::TAB() . parent::TAB() . 'if($idgen->isAfterInsert())' . self::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$id = $idgen->getId();' . self::LN();
            */
		}

		$content .= parent::TAB() . parent::TAB() . 'return $id;' . self::LN();

		$content .= parent::TAB() . '}' . self::LN();

		/*
		$content .= self::LN();
		$content .= parent::TAB().parent::TAB().'// datensatz aus datenbank holen - alle änderungen müssen übnernommen werden'.self::LN();
		$pk_string = array();
		$pk_attr = array();
		foreach ($myTable->getPrimaryKey()->getColumns() as $column){
			$pk_string[] = strtolower($myTable->getName()).'.'.$column->getName().' = ?';
			$pk_attr[] = '$myObject->get'.self::formatAttributename($column->getName()).'()';
		}
		$content .= parent::TAB().parent::TAB().'$myList = self::get'.self::makeClassName($myTable->getName()).'ListByQuery(\''.implode(' AND ', $pk_string).'\', array('.( count($myTable->getPrimaryKey()->getColumns()) > 1 ? implode(', ', $pk_attr) : '$id' ) .'), new SQLLimit(1));'.self::LN();
		$content .= parent::TAB().parent::TAB().'$myNewObject = $myList->current();'.self::LN();

		foreach ( $myTable->getColumns() as $col )
			$content .= parent::TAB().parent::TAB().'$myObject->set'.parent::formatAttributename($col->getName()).'($myNewObject->get'.parent::formatAttributename($col->getName()).'()); // overwrite original'.self::LN();

		$content .= parent::TAB().parent::TAB().'$myObject->_setIsNew(false);'.self::LN();
		$content .= parent::TAB().'}'.self::LN();
		$content .= self::LN();
		*/

		return $content;
	}

	private static function buildUpdate ( DBTable $myTable, $fk_tables )
	{
		$content = parent::TAB() . 'private static function update(' . self::makeClassName( $myTable->getName() ) . ' $myObject): void' . self::LN();
        $content .= parent::TAB() . '{' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$updates = array();' . self::LN();
		foreach ( $myTable->getColumns() as $col )
		{
			$col instanceof DBColumn;
			$value = '?';
			if ( $col->getType() == DBTypes::TIMESTAMP or preg_match( '/^tstamp\_/ui', $col->getName() ) )
				$value = 'FROM_UNIXTIME(?)';
			if ( $col->getType() == DBTypes::DATE )
				$value = 'FROM_UNIXTIME(?)';
			if ( $col->getType() == DBTypes::BINARY )
				$value = 'BIN_TO_UUID(?)';
			$content .= parent::TAB() . parent::TAB() . 'if( $myObject->_getIsModified(\'' . $col->getName() . '\') ) {' . self::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$updates[] = \'' . strtolower( $myTable->getName() ) . '.' . $col->getName() . ' = ' . $value . '\';' . self::LN();

			// aktuellen zeitstempel setzen
			if ( preg_match( '/^tstamp\_modified/ui', $col->getName() ) )
			{
				$content .= parent::TAB() . parent::TAB() . '}else{' . self::LN();
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$updates[] = \'' . strtolower( $myTable->getName() ) . '.' . $col->getName() . ' = NOW()\';' . self::LN();
			}
			$content .= parent::TAB() . parent::TAB() . '}' . self::LN();
		}
		$content .= self::LN();
		$content .= parent::TAB() . parent::TAB() . '$conn = DBConnection::getConnection();' . self::LN();
		$pk_string = array();
		$pk_attr = array();
		$pk_lines = parent::TAB() . parent::TAB() . '// pk' . self::LN();
		foreach ( $myTable->getPrimaryKey()->getColumns() as $column )
		{
			$pk_string[] = strtolower( $myTable->getName() ) . '.' . $column->getName() . ' = ?';
			$type = DBCreoleTypes::getCreoleFunctionNames( self::getPHPType( $column->getTypname() ) );
			$pk_attr[] = '$myObject->get' . self::formatAttributename( $column->getName() ) . '()';
			$pk_lines .= parent::TAB() . parent::TAB() . '$stmt->set' . $type . '($paramCount++, $myObject->get' . self::formatAttributename( $column->getName() ) . '());' . self::LN();
		}

		$content .= parent::TAB() . parent::TAB() . '$where = \'UPDATE \'.self::TABLENAME.\' SET \'.implode(\', \', $updates). \' WHERE ' . implode( ' AND ', $pk_string ) . '\';' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$stmt = $conn->prepareStatement($where);' . self::LN();
		$content .= parent::TAB() . parent::TAB() . '$paramCount = 1;' . self::LN();

		foreach ( $myTable->getColumns() as $col )
		{
			$type = DBCreoleTypes::getCreoleFunctionNames( self::getPHPType( $col->getTypname() ) );
			$content .= parent::TAB() . parent::TAB() . 'if( $myObject->_getIsModified(\'' . $col->getName() . '\') )' . self::LN();
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . '$stmt->set' . $type . '($paramCount++, $myObject->get' . self::formatAttributename( $col->getName() ) . '());' . self::LN();
		}
		$content .= $pk_lines;
		$content .= self::LN();
		$content .= parent::TAB() . parent::TAB() . '$stmt->executeUpdate();' . self::LN();
		$content .= self::LN();

		/*
		$content .= parent::TAB().parent::TAB().'// veränderten datensatz aus db laden (tstamp haben sich geändert)'.self::LN();
		$content .= parent::TAB().parent::TAB().'$myList = self::get'.self::makeClassName($myTable->getName()).'ListByQuery(\''.implode(' AND ', $pk_string).'\', array('.( count($myTable->getPrimaryKey()->getColumns()) > 1 ? implode(', ', $pk_attr) : $pk_attr[0] ) .'), new SQLLimit(1));'.self::LN();
		$content .= parent::TAB().parent::TAB().'$myObject = $myList->current(); // overwrite orginal'.self::LN();
		*/

		$content .= parent::TAB() . '}' . self::LN();
		$content .= self::LN();


		return $content;
	}

}