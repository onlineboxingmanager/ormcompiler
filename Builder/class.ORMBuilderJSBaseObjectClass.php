u<?php
#ä
require_once 'class.ORMBuilderBase.php';

abstract class ORMBuilderJSBaseObjectClass extends ORMBuilderBase
{

	protected static array $imports = [];
	protected static array $relations = [];
	protected static array $relation_cast = [];

	public static function build( ORMConfig $myConfig, DBTable $myTable, array $fk_tables = NULL )
	{
		self::setConfig($myConfig);

		self::$imports = [];
		self::$relations = [];
		self::$relation_cast = [];

		$myContent = 'import {' . self::makeClassName($myTable->getName()) . 'DAO} from "./' . self::makeClassName($myTable->getName()) . 'DAO"' . PHP_EOL;

		$myContent .= '###IMPORTS' . PHP_EOL . PHP_EOL;

		$myContent .= 'export abstract class ' . self::makeClassName($myTable->getName()) . 'Base extends ' . self::makeClassName($myTable->getName()) . 'DAO' . PHP_EOL;
		$myContent .= '{' . PHP_EOL . PHP_EOL;

		$myContent .= self::buildAttributes($myTable, $fk_tables);
		$myContent .= self::buildMethods($myTable, $fk_tables);
		$myContent .= parent::FOOTER();

		$filename = '' . self::makeClassName($myTable->getName()) . 'Base.ts';
		$path = $myConfig->getJsPath() . '/models/';
		if ( substr($path, -1) == '/' )
			$path = substr($path, 0, -1);
		$myName = str_replace([
			'new_',
			'tbl_',
		], '', strtolower($myTable->getName()));
		$myFolder = explode('_', $myName);
		foreach ( $myFolder as $folder )
		{
			$path .= '/' . parent::formatName($folder);
		}
		$path_folder = $path;
		$path = $path . '/' . $filename;

		// replace import
		$myContent = str_replace('###IMPORTS', implode(PHP_EOL, self::$imports), $myContent);

		self::write($path, $myContent);

		$object_path = $myConfig->getJsPath() . '/models/' . self::makeClassName($myTable->getName()) . '.ts';
		if ( !file_exists($object_path) )
			ORMBuilderJSObjectClass::build($myConfig, $myTable);
	}

	private static function buildAttributes( DBTable $myTable, array $fk_tables = NULL )
	{
		$content = parent::TAB() . "// references" . PHP_EOL;

		// new method
		$fk_columns = [];
		$ref_list = [];
		if ( isset($fk_tables[$myTable->getName()]) and !in_array($myTable->getName(), $fk_tables['unassigned']) )
		{
			foreach ( $fk_tables[$myTable->getName()] as $reference )
			{
				$classname = $reference['table'];

				$is_nullable = ( $reference['nullable'] == TRUE ? ' = null' : NULL );

				$referencetype = '';

				$is_list = FALSE;
				if ( $reference['type'] == DBReferenceTypes::REFERENCE_MANY_TO_MANY or $reference['type'] == DBReferenceTypes::REFERENCE_SINGLE_TO_MANY )
				{
					$referencetype = '_list';
					//$classname .= 'List';
					$is_list = TRUE;
				}

				if ( $is_list )
				{
					if ( $is_nullable )
						$content .= parent::TAB() . 'protected ref_' . $reference['referenz_name_org'] . $referencetype . ': Map<number, ' . self::makeClassName($classname) . '>|null = null' . PHP_EOL;
					else
						$content .= parent::TAB() . 'protected ref_' . $reference['referenz_name_org'] . $referencetype . ': Map<number, ' . self::makeClassName($classname) . '> = new Map<number, ' . self::makeClassName($classname) . '>' . PHP_EOL;
				}

				else
				{
					if ( $is_nullable )
						$content .= parent::TAB() . 'protected ref_' . $reference['referenz_name_org'] . $referencetype . ': ' . self::makeClassName($classname) . '|null = null' . PHP_EOL;
					else
						$content .= parent::TAB() . 'protected ref_' . $reference['referenz_name_org'] . $referencetype . ': ' . self::makeClassName($classname) . ' = new ' . self::makeClassName($classname) . '()' . PHP_EOL;
				}

				$filename = '' . self::makeClassName($reference['referenz_name_org']) . '';
				$path = '@/models';
				if ( substr($path, -1) == '/' )
					$path = substr($path, 0, -1);
				$myName = str_replace([
					'new_',
					'tbl_',
				], '', strtolower($reference['table']));
				$myFolder = explode('_', $myName);
				foreach ( $myFolder as $folder )
				{
					$path .= '/' . parent::formatName($folder);
				}
				$path_folder = $path;
				$path = $path . '/' . $filename;

				self::$imports[$path] = 'import {' . self::makeClassName($reference['referenz_name_org']) . '} from "' . $path . '"';
				self::$relations[$path] = parent::TAB() . parent::TAB() . 'ref_' . $reference['referenz_name_org'] . ( $is_list ? '_list' : '') . ': \''. self::makeClassName($reference['referenz_name_org']).'\',';

				self::$relation_cast[$path] = parent::TAB() . 'public castTo'.self::makeClassName($reference['referenz_name_org']).'(obj): ' . self::makeClassName($reference['referenz_name_org']) . PHP_EOL;
				self::$relation_cast[$path] .= parent::TAB() . '{'. PHP_EOL;
				self::$relation_cast[$path] .= parent::TAB() . parent::TAB() . 'return (new '.self::makeClassName($reference['referenz_name_org']).'()).loadFromObject(obj)'. PHP_EOL;

				self::$relation_cast[$path] .= parent::TAB() . '}'. PHP_EOL;

				$fk_columns[] = self::makeRefName($reference['col_own']);

				$ref_list[$reference['col_own']] = $reference['referenz_name_org'];
			}
		}

		$content .= PHP_EOL;
		$content .= parent::TAB() . 'protected static relations = {'. PHP_EOL . '###RELATIONS' . PHP_EOL;
		$content .= parent::TAB() . '}'. PHP_EOL;

		// replace import
		$content = str_replace('###RELATIONS', implode(PHP_EOL, self::$relations), $content);
		$content .= PHP_EOL;

		// cast functions
		$content .= implode(PHP_EOL, self::$relation_cast);
		$content .= PHP_EOL;


		$content .= PHP_EOL;
		$content .= parent::TAB() . "// attributes" . PHP_EOL;
		foreach ( $myTable->getColumns() as $column )
		{
			/** @var DBColumn $column */

			$typename = parent::getTypescriptType(strtolower(DBTypes::getCreoleName($column->getType())));

			$defaultvalue = ( ( is_numeric($column->getDefaultvalue()) or $typename == 'int' ) ? (int) $column->getDefaultvalue() : '\'' . $column->getDefaultvalue() . '\'' );

			if ( $typename == 'number' )
				$defaultvalue = (float) $column->getDefaultvalue();

			if ( $typename == 'number' and empty($defaultvalue) )
				$defaultvalue = 'null';

			if ( $typename == 'number' and $defaultvalue == 'null' and !$column->getIsNullable() )
				$defaultvalue = '0';

			if ( $typename == 'Date' and ( empty($defaultvalue) or stristr($defaultvalue, 'current_timestamp') ) )
				$defaultvalue = 'new Date()';

			if ( $column->getIsNullable() )
				$content .= parent::TAB() . 'private _' . strtolower($column->getName()) . ': ' . $typename . '|null = null;' . PHP_EOL;
			else
				$content .= parent::TAB() . 'private _' . strtolower($column->getName()) . ': ' . $typename . ' = ' . $defaultvalue . ';' . PHP_EOL;
		}
		$content .= PHP_EOL;

		return $content;
	}

	private static function buildMethods( DBTable $myTable, array $fk_tables = NULL )
	{
		// foreignkeys - references
		$content = parent::TAB() . '/**************************** REFERENCES ****************************/' . PHP_EOL;

		// new method
		$fk_columns = [];
		if ( isset($fk_tables[$myTable->getName()]) and !in_array($myTable->getName(), $fk_tables['unassigned']) )
		{
			foreach ( $fk_tables[$myTable->getName()] as $reference )
			{
				$attributename = $reference['referenz_name_org'];

				//$reference_name = $reference['referenz_name'];
				$reference_name = $reference['referenz_name_org'];

				$classname = $reference['classname'];
				$fk_columns[] = $attributename;

				$isMany = FALSE;
				if ( $reference['type'] == DBReferenceTypes::REFERENCE_MANY_TO_MANY or $reference['type'] == DBReferenceTypes::REFERENCE_SINGLE_TO_MANY )
				{
					$attributename .= '_list';
					//$classname .= 'List';
					$isMany = TRUE;
				}
				$is_nullable = ( $reference['nullable'] == TRUE ? ' = null' : NULL );

				$nullable = ( $is_nullable ? '|null' : '' );

				$content .= parent::TAB() . 'public ' . self::makeAttSetFunction($attributename) . '(' . 'myObject: ' . ( $isMany ? 'Map<number, ' . $classname . '>' : $classname ) . $nullable . '): void' . PHP_EOL;
				$content .= parent::TAB() . '{' . PHP_EOL;
				$content .= parent::TAB() . parent::TAB() . 'this.ref_' . strtolower($attributename) . ' = myObject;' . PHP_EOL;
				if ( $isMany != TRUE )
				{
					if ( $is_nullable != NULL )
						$content .= parent::TAB() . parent::TAB() . 'if( myObject )' . PHP_EOL;

					$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'this._' . strtolower($reference['col_own_attributename']) . ' = myObject.' . strtolower($reference['col_ref_attributename']) . '' . PHP_EOL;
				}

				if ( $isMany )
					$content .= parent::TAB() . parent::TAB() . 'this._setIsLoaded(\'ref_' . strtolower($attributename) . '\')' . PHP_EOL;

				$content .= parent::TAB() . '}' . PHP_EOL . PHP_EOL;

				$content .= parent::TAB() . 'public '.( $isMany ? 'async ' : '' ). self::makeAttGetFunction($reference_name, $isMany) . ': ' . ( $isMany ? 'Promise<Map<number, ' . $classname . '>>' : $classname ) . ( $is_nullable ? '|null' : '' ) . PHP_EOL;
				$content .= parent::TAB() . '{' . PHP_EOL;

				//$content .= parent::TAB() . parent::TAB() . 'if( !ref_' . strtolower( $attributename ) . ' )' . PHP_EOL;
				//$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'this.' . $attributename  . '(' . self::makeClassName( $myTable->getName() ) . 'Manager::get' . self::makeClassName( $reference_name ) . ( $isMany ? 'List' : '' ) . 'By' . self::makeClassName( $myTable->getName() ) . '($this' . ( $isMany ? ', $myLimit' : '' ) . '));' . PHP_EOL;

				if ( $isMany )
				{
					$content .= parent::TAB() . parent::TAB() . 'if ( is_force || !this._getIsLoaded(\'ref_' . strtolower($attributename) . '\') )' . PHP_EOL;
					$content .= parent::TAB() . parent::TAB() . '{' . PHP_EOL;

					$content .= parent::TAB() . parent::TAB() . parent::TAB() . '// @ts-ignore' . PHP_EOL;
					$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'this.' . self::makeAttSetFunction($attributename) . '(await ' . $classname . '.getListByReference(this))' . PHP_EOL;

					$content .= parent::TAB() . parent::TAB() . '}' . PHP_EOL;
				}

				$content .= parent::TAB() . parent::TAB() . 'return this.ref_' . strtolower($attributename) . ';' . PHP_EOL;
				$content .= parent::TAB() . '}' . PHP_EOL . PHP_EOL;
			}
		}

		// attribute setter and getter
		$content .= parent::TAB() . '/**************************** ATTRIBUTES ****************************/' . PHP_EOL;
		foreach ( $myTable->getColumns() as $column )
		{
			/** @var DBColumn $column */

			// skip "single-to-single" reference - [Nur Objektzugriff erlaubt]
			if ( in_array($column->getName(), $fk_columns) )
				continue;

			$typename = parent::getTypescriptType(strtolower(DBTypes::getCreoleName($column->getType())));

			$null_check = 'is_null($' . $typename . ')'; // default null check
			$attr_check = 'is_' . self::getPHPType($typename) . '($' . $typename . ')' . ( $column->getIsNullable() == TRUE ? ' OR is_null($' . $typename . ')' : NULL ) . '';

			// setter

			$content .= parent::TAB() . 'public set ' . $column->getName() . '( value: ' . $typename . ( $column->getIsNullable() ? '|null' : '' ) . ')' . PHP_EOL;
			$content .= parent::TAB() . '{' . PHP_EOL;

			$content .= parent::TAB() . parent::TAB() . 'if( this._' . strtolower($column->getName()) . ' !== value )' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . '{' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'this._' . strtolower($column->getName()) . ' = value' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'this._setModified(\'' . strtolower($column->getName()) . '\')' . PHP_EOL;

			$content .= parent::TAB() . parent::TAB() . '}' . PHP_EOL;

			$content .= parent::TAB() . '}' . PHP_EOL;
			$content .= PHP_EOL;

			// getter
			$content .= parent::TAB() . 'public get ' . strtolower($column->getName()) . '(): ' . $typename . ( $column->getIsNullable() ? '|null' : '' ) . PHP_EOL;
			$content .= parent::TAB() . '{' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . 'return this._' . strtolower($column->getName()) . '' . PHP_EOL;
			$content .= parent::TAB() . '}' . PHP_EOL;
			$content .= PHP_EOL;
		}

		return $content;
	}

	private static function makeAttSetFunction( $attname )
	{
		$attname = 'set' . parent::formatAttributename(strtolower($attname));
		//$attname = 'set ' . strtolower( $attname );
		return $attname;
	}

	private static function makeAttGetFunction( $attname, $isMany = FALSE )
	{
		//$attname = 'get ' . parent::formatAttributename( strtolower( $attname ) ) . ( $isMany == true ? 'List(SQLLimit $myLimit = null)' : '()' );
		$attname = 'get' . parent::formatAttributename(strtolower($attname)) . ( $isMany ? 'List(is_force: boolean = false)' : '()' );
		return $attname;
	}

}