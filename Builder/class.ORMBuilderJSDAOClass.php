<?php
#ä
require_once 'class.ORMBuilderBase.php';

abstract class ORMBuilderJSDAOClass extends ORMBuilderBase
{

	public static function build ( ORMConfig $myConfig, DBTable $myTable, array $fk_tables, array $m2m_tables ): void
	{
		self::setConfig( $myConfig );
		$myContent = 'import {BaseDAO} from "@/models/BaseDAO"' . PHP_EOL . PHP_EOL;

		$myContent .= 'export abstract class ' . self::makeClassName( $myTable->getName() ) . 'DAO extends BaseDAO' . PHP_EOL;
        $myContent .= '{' . PHP_EOL;

		$myContent .= parent::TAB() . 'public static CLASSNAME: string = \'' . preg_replace('/^tbl_/ui' , '', strtolower( $myTable->getName() )) . '\';' . PHP_EOL . PHP_EOL;
		$myContent .= self::buildPrimaryKeys( $myTable );
		$myContent .= self::buildColumns( $myTable, $fk_tables );
		$myContent .= parent::FOOTER();

		$filename = '' . self::makeClassName( $myTable->getName() ) . 'DAO.ts';
		$path = $myConfig->getJsPath() . '/models/';
		if ( substr( $path, -1 ) == '/' )
			$path = substr( $path, 0, -1 );
		$myName = str_replace( array( 'new_', 'tbl_' ), '', strtolower( $myTable->getName() ) );
		$myFolder = explode( '_', $myName );
		foreach ( $myFolder as $folder )
		{
			$path .= '/' . parent::formatName( $folder );
		}
		$path_folder = $path;
		$path = $path . '/' . $filename;

		self::write( $path, $myContent );
	}

	private static function buildPrimaryKeys ( DBTable $myTable ): string
	{
        $content = parent::TAB() . 'protected static primarykeys = {' . PHP_EOL;
		$pk = array();
		foreach ( $myTable->getPrimaryKey()->getColumns() as $column )
		{
			$pk[] = parent::TAB() . parent::TAB() . $column->getName() . ': \'get' . self::formatAttributename( $column->getName() ) . '\'';
		}
		$content .= implode( ', ' . PHP_EOL, $pk ) . PHP_EOL;
		$content .= parent::TAB() . '}' . PHP_EOL;
		$content .= PHP_EOL;

		return $content;
	}

	private static function buildColumns ( DBTable $myTable, array $fk_tables ): string
	{
		$content = '';

		$content .= parent::TAB() . "constructor()" . PHP_EOL;
		$content .= parent::TAB() . "{" . PHP_EOL;
		$content .= parent::TAB() . parent::TAB() . "super()" . PHP_EOL;
		$content .= parent::TAB() . parent::TAB() . "this.setClassname('" . preg_replace('/^tbl_/ui', '', $myTable->getName()) . "')" . PHP_EOL;
		$content .= parent::TAB() . parent::TAB() . "this._setProperties({" . PHP_EOL;
		foreach ( $myTable->getColumns() as $column )
		{
            /**
             * @var DBColumn $column
             */

			if ( !$column->getType() )
			{
				echo "Typ nicht gefunden - tabelle: " . $myTable->getName();
				var_dump( $column );
				die();
			}

			$content .= parent::TAB() . parent::TAB() . '' . strtolower( $column->getName() ) . ': {' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'size: ' . $column->getSize() . ',' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'type: ' . $column->getType() . ',' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'typename: \'' . $column->getTypname() . '\',' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'default: ' . ( !is_null($column->getDefaultvalue()) ? '\'' . $column->getDefaultvalue() . '\'' : 'null' ) . ',' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'is_autoincrement: ' . ( $column->getIsAutoIncrement() ? 'true' : 'false' ) . ',' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'is_nullable: ' . ( $column->getIsNullable() ? 'true' : 'false' ) . ',' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'comment: \'' . $column->getComment() . '\',' . PHP_EOL;

			// is pk
			if ( $myTable->getPrimaryKey()->getName() == $column->getName() )
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'is_primarykey: true,' . PHP_EOL;
			else
			{
				$content .= parent::TAB() . parent::TAB() . parent::TAB() . 'is_primarykey: false,' . PHP_EOL;
			}

            $reference_attribute = 'null';

            // if reference
            if ( isset( $fk_tables[ $myTable->getName() ] ) AND $myTable->getPrimaryKey()->getName() != $column->getName() )
            {
                foreach ( $fk_tables[$myTable->getName()] as $reference )
                {
                    if( isset($reference['col_ref']) AND $reference['col_ref'] === $column->getName() )
                    {
                        $reference_attribute = '\''.$reference['referenz_name_org'].'\'';
                    }
                }
            }

            $content .= parent::TAB() . parent::TAB() . parent::TAB() . 'reference: '.$reference_attribute.',' . PHP_EOL;
			$content .= parent::TAB() . parent::TAB() . '},' . PHP_EOL;
		}
		$content .= parent::TAB() . parent::TAB() . "})" . PHP_EOL;
		$content .= parent::TAB() . "};" . PHP_EOL;

		return $content;
	}

}