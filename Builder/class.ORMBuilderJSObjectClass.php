<?php

##
require_once 'class.ORMBuilderBase.php';

abstract class ORMBuilderJSObjectClass extends ORMBuilderBase
{

    public static function build(ORMConfig $myConfig, DBTable $myTable, array $fk_tables = null)
    {
        self::setConfig($myConfig);
        $myContent = 'import {'.self::makeClassName($myTable->getName()).'Base} from "./'.self::makeClassName($myTable->getName()).'Base"' . PHP_EOL . PHP_EOL;
        $myContent .= 'export class ' . self::makeClassName($myTable->getName()) . ' extends ' . self::makeClassName($myTable->getName()) . 'Base { ' . parent::LN() . parent::LN() . parent::LN();
        $myContent .= parent::FOOTER();

        $filename = '' . self::makeClassName($myTable->getName()) . '.ts';
        $path = $myConfig->getJsPath() . '/models/';
        if (substr($path, -1) == '/') {
            $path = substr($path, 0, -1);
        }
        $myName = str_replace(['new_', 'tbl_'], '', strtolower($myTable->getName()));
        $myFolder = explode('_', $myName);
        foreach ($myFolder as $folder) {
            $path .= '/' . parent::formatName($folder);
        }
        $path = $path . '/' . $filename;

	    if( !file_exists($path) )
            self::write($path, $myContent);
    }

}