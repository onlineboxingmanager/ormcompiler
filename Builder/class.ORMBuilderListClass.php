<?php
#�
require_once 'class.ORMBuilderBase.php';

abstract class ORMBuilderListClass extends ORMBuilderBase {

	public static function build(ORMConfig $myConfig, DBTable $myTable)
    {
		self::setConfig($myConfig);

		$myContent  = parent::HEADER();

	    if( self::$config->getIsUseNamespaces() )
	    {
		    $myContent .= 'namespace Enduron\Models\\'.self::makeClassName($myTable->getName()).';' . parent::LN();
		    $myContent .= 'use Enduron\Core\Utilities\BaseList;' . parent::LN();
		    $myContent .= 'use Enduron\Core\Utilities\UUID;' . parent::LN();
	    }
	    else
	    {
		    $myContent .= 'Library::requireLibrary(LibraryKeys::SYSTEM_UTILITIES_LIST());'.parent::LN();
	    }

		$myContent .= parent::LN();

        $objectname = self::makeClassName($myTable->getName());
        $additional = parent::LN();
        $additional .= ' * @method '.$objectname.' current()'.parent::LN();
        $additional .= ' * @method \Generator<'.$objectname.'> getIterator()'.parent::LN();
        $additional .= ' * @method void add('.$objectname.' $myObject)'.parent::LN();
        $additional .= ' * @implements \IteratorAggregate<'.$objectname.'>'.parent::LN();

		$myContent .= parent::COMMENT(self::makeClassName($myTable->getName()) . ' List', $additional);
		$myContent .= 'class '.self::makeClassName($myTable->getName()).'List extends BaseList'.parent::LN();
        $myContent .= '{'.parent::LN().parent::LN();

		$myContent .= parent::TAB().'protected string $objecttype = \''.self::makeClassName($myTable->getName()).'\';'.parent::LN();

		// get
        /*
        $myContent .= parent::TAB().'#[ReturnTypeWillChange]'.parent::LN();
		$myContent .= parent::TAB().'public function current(): '.self::makeClassName($myTable->getName()).parent::LN();
        $myContent .= parent::TAB().'{'.parent::LN();
		//$myContent .= parent::TAB().parent::TAB().'return $this->list[($this->pos == 0 ? $this->pos : ($this->pos - 1))];'.parent::LN();
		$myContent .= parent::TAB().parent::TAB().'return parent::current();'.parent::LN();
		$myContent .= parent::TAB().'}'.parent::LN();
		$myContent .= parent::LN();

		// add
		$myContent .= parent::TAB().'public function add('.self::makeClassName($myTable->getName()).'|BaseObject $myObject): void'.parent::LN();
        $myContent .= parent::TAB().'{'.parent::LN();
		$myContent .= parent::TAB().parent::TAB().'parent::add($myObject);'.parent::LN();
		//$myContent .= parent::TAB().parent::TAB().'$this->list[] = $myObject;'.parent::LN();
		//$myContent .= parent::TAB().parent::TAB().'$this->pos = (count($this->list)-1);'.parent::LN();
		$myContent .= parent::TAB().'}'.parent::LN();
		$myContent .= parent::LN();
        */

		$myContent .= parent::FOOTER();

		$filename = ( self::$config->getIsUseNamespaces() ? '' : 'class.' ).self::makeClassName($myTable->getName()).'List.php';
		$path = $myConfig->getApplicationPath();
		if( substr($path, -1) == '/' )
			$path = substr($path, 0, -1);
		$myName = str_replace(array('new_', 'tbl_'), '', strtolower($myTable->getName()));

	    if( self::$config->getIsUseNamespaces() )
		    $myName = str_replace('_', '', ucwords($myName, '_'));

		$myFolder = explode('_', $myName);
		foreach ($myFolder as $folder){
			$path .= '/'.parent::formatName($folder);
		}
		$path = $path.'/'.$filename;

		self::write($path,$myContent);
	}
}