<?php

##
require_once 'class.ORMBuilderBase.php';

abstract class ORMBuilderObjectClass extends ORMBuilderBase
{

    public static function build(ORMConfig $myConfig, DBTable $myTable, array $fk_tables = null)
    {
        self::setConfig($myConfig);
        $myContent = parent::HEADER();

	    if( self::$config->getIsUseNamespaces() )
	    {
		    $myContent .= 'namespace Enduron\Models\\' . self::makeClassName($myTable->getName()) . ';' . parent::LN();
		    $myContent .= 'use Enduron\Models\\'.self::makeClassName($myTable->getName()).'\\'.self::makeClassName($myTable->getName()) . 'Manager;' . parent::LN();
		    $myContent .= 'use Enduron\Models\\'.self::makeClassName($myTable->getName()).'\\'.self::makeClassName($myTable->getName()) . 'Base;' . parent::LN();
		    $myContent .= 'use Enduron\Core\DBAL\SQLLimit;' . parent::LN();
		    $myContent .= 'use Enduron\Core\Utilities\UUID;' . parent::LN();
		    $myContent .= parent::LN();
	    }

        $myContent .= parent::COMMENT(self::makeClassName($myTable->getName()) . ' Object');
        $myContent .= 'class ' . self::makeClassName($myTable->getName()) . ' extends ' . self::makeClassName($myTable->getName()) . 'Base { ' . parent::LN() . parent::LN() . parent::LN();
        $myContent .= parent::FOOTER();

        $filename = ( self::$config->getIsUseNamespaces() ? '' : 'class.' ) . self::makeClassName($myTable->getName()) . '.php';
        $path = $myConfig->getApplicationPath();
        if (substr($path, -1) == '/') {
            $path = substr($path, 0, -1);
        }
        $myName = str_replace(['new_', 'tbl_'], '', strtolower($myTable->getName()));

	    if( self::$config->getIsUseNamespaces() )
		    $myName = str_replace('_', '', ucwords($myName, '_'));

        $myFolder = explode('_', $myName);
        foreach ($myFolder as $folder) {
            $path .= '/' . parent::formatName($folder);
        }
        $path = $path . '/' . $filename;

		if( !file_exists($path) )
            self::write($path, $myContent);
    }

}