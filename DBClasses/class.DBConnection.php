<?php
#ä

abstract class DBConnection
{

	public static \Connection $connection;

	public static final function getConnection( ORMConfig $myConfig ): \Connection
	{
		if ( !empty(self::$connection) )
			return self::$connection;

		require_once $myConfig->getCreolePath();

		$dsn = [
			'phptype'  => $myConfig->getDbDriver(),
			'hostspec' => $myConfig->getDbHost(),
			'username' => $myConfig->getDbLoginname(),
			'password' => $myConfig->getDbPassword(),
			'database' => $myConfig->getDbDatabase(),
			'encoding' => $myConfig->getDbCharset() ?? null,
			'port'      => $myConfig->getDbPort() ?? 3306
		];

		#$conn = Creole::getConnection($dsn, Creole::PERSISTENT);
		$conn = Creole::getConnection($dsn, Creole::COMPAT_ASSOC_LOWER);
		if ( !( $conn->getAutoCommit() === FALSE ) )
		{
			$conn->setAutoCommit(FALSE);
		}

		#$conn->begin(); // start transaction
		self::$connection = $conn;
		return $conn;
	}

	/**
	 * Overwrites stored Connection with a new one
	 */
	function disconnect()
	{
		DBConnection::getConnection()->close();
	}

	/**
	 * Commits Transaction in the stored connection.
	 * Throws SQLException, if the stored connection is NULL.
	 */
	function commit()
	{
		DBConnection::getConnection()->commit();
	}

	function rollback()
	{
		DBConnection::getConnection()->rollback();
	}
}