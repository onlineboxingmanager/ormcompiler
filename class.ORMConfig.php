<?php
#ä
require_once 'class.ORMBase.php';

class ORMConfig extends ORMBase
{

	private string $abstraction_path;
	private string $application_path;

	private string $db_loginname;
	private string $db_password;
	private bool   $generate_js_classes     = false;
	private bool   $include_system          = false;
	private bool   $is_pdo                  = false;
	private string $js_path                 = '';
	private bool   $reference_is_unassigned = false;
	private string $system_path;
	private bool   $is_use_namespaces          = true;

	public function getIsUseNamespaces(): bool
	{
		return $this->is_use_namespaces;
	}

	public function setIsUseNamespaces(bool $use_namespaces): void
	{
		$this->is_use_namespaces = $use_namespaces;
	}

	// attributes
	private int    $count           = 0;
	private int    $count_must_have = 12;
	private string $creole_path;
	private string $db_charset;
	private string $db_database;
	private string $db_driver;
	private string $db_host;
	private string $db_port;

	public function getDbPort (): string
	{
		return $this->db_port;
	}

	public function setDbPort ( string $db_port ): void
	{
		$this->db_port = $db_port;
	}

	public function getAbstractionPath (): string
	{
		return $this->abstraction_path;
	}

	public function setAbstractionPath ( string $string )
	{
		$this->abstraction_path = $string;
		$this->count++;
	}

	public function getApplicationPath (): string
	{
		return $this->application_path;
	}

	public function setApplicationPath ( string $string )
	{
		$this->application_path = $string;
		$this->count++;
	}

	public function getCreolePath (): string
	{
		return $this->creole_path;
	}

	public function setCreolePath ( string $string )
	{
		$this->creole_path = $string;
		$this->count++;
	}

	public function getDbCharset (): string
	{
		return $this->db_charset;
	}

	public function setDbCharset ( string $string )
	{
		$this->db_charset = $string;
		$this->count++;
	}

	public function getDbDatabase (): string
	{
		return $this->db_database;
	}

	public function setDbDatabase ( string $string )
	{
		$this->db_database = $string;
		$this->count++;
	}

	public function getDbDriver (): string
	{
		return $this->db_driver;
	}

	public function setDbDriver ( string $string )
	{
		$this->db_driver = $string;
		$this->count++;
	}

	public function getDbHost (): string
	{
		return $this->db_host;
	}

	public function setDbHost ( string $string )
	{
		$this->db_host = $string;
		$this->count++;
	}

	public function getDbLoginname (): string
	{
		return $this->db_loginname;
	}

	public function setDbLoginname ( $string )
	{
		$this->db_loginname = $string;
		$this->count++;
	}

	public function getDbPassword (): string
	{
		return $this->db_password;
	}

	public function setDbPassword ( string $string )
	{
		$this->db_password = $string;
		$this->count++;
	}

	public function getIncludeSystem (): bool
	{
		return $this->include_system;
	}

	public function setIncludeSystem ( bool $bool ): void
	{
		$this->include_system = $bool;
		$this->count++;
	}

	public function getIsPdo (): bool
	{
		return $this->is_pdo;
	}

	public function setIsPdo ( bool $is_pdo ): void
	{
		$this->is_pdo = $is_pdo;
	}

	public function getJsPath (): string
	{
		return $this->js_path;
	}

	public function setJsPath ( string $js_path ): void
	{
		$this->js_path = $js_path;
	}

	public function getReferenceIsUnassigned (): bool
	{
		return $this->reference_is_unassigned;
	}

	public function setReferenceIsUnassigned ( bool $bool )
	{
		$this->reference_is_unassigned = $bool;
		$this->count++;
	}

	public function getSystemPath (): string
	{
		return $this->system_path;
	}

	public function setSystemPath ( string $string )
	{
		$this->system_path = $string;
		$this->count++;
	}

	public function isGenerateJsClasses (): bool
	{
		return $this->generate_js_classes;
	}

	public function setGenerateJsClasses ( bool $generate_js_classes ): void
	{
		$this->generate_js_classes = $generate_js_classes;
	}

	public function isValid (): bool
	{
		return ( $this->getCount() === $this->getCountMustHave() );
	}

	public function getCount (): int
	{
		return $this->count;
	}

	public function setCount ( int $count ): void
	{
		$this->count = $count;
	}

	public function getCountMustHave (): int
	{
		return $this->count_must_have;
	}

	public function setCountMustHave ( int $count_must_have ): void
	{
		$this->count_must_have = $count_must_have;
	}

	public function reset (): void
	{
		$this->count = 0;
		$this->count_must_have = 12;
	}

}