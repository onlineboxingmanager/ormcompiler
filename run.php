<?php
#ä
set_time_limit(0);
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_DEPRECATED);

$myPath = $_SERVER['SCRIPT_FILENAME'];
define('ORMCOMPILER_PATH', __DIR__);

if( class_exists('PDO') AND in_array('mysql', PDO::getAvailableDrivers()) )
	echo "\033[33m" . '"MySQL für PDO" ist aktiviert und kann benutzt werden!' . PHP_EOL;

require_once __DIR__ . '/class.ORMBase.php';
include_once __DIR__ . '/class.ORMConfig.php'; // serialize
require_once __DIR__ . '/Builder/ORMCompiler.php';

$myConfig = new ORMConfig();
$myConfig->setCreolePath(__DIR__ . '/creole/Creole.php');
$myConfig->setDbDriver(getenv('DB_DRIVER'));
$myConfig->setDbDatabase(getenv('DB_DATABASE'));
$myConfig->setDbHost(getenv('DB_HOST'));
$myConfig->setDbCharset('utf8');
$myConfig->setIsPdo(true);
$myConfig->setDbPort(getenv('DB_PORT'));
$myConfig->setDbLoginname(getenv('DB_USERNAME'));
$myConfig->setDbPassword(getenv('DB_PASSWORD'));
$myConfig->setApplicationPath(getenv('ORMCOMPILER_OUTPUT_MODELS') ?? __DIR__ . '/../../../application/models');
$myConfig->setAbstractionPath( $myConfig->getApplicationPath() );
$myConfig->setSystemPath( __DIR__ . '/../../../application/system' );
$myConfig->setJsPath( getenv('ORMCOMPILER_OUTPUT_JS') ?? __DIR__ . '/../../../src' );
$myConfig->setGenerateJsClasses( true );
$myConfig->setIncludeSystem(0);
$myConfig->setReferenceIsUnassigned(TRUE);
$myConfig->setIsUseNamespaces(getenv('ORMCOMPILER_USE_NAMESPACES') ?? TRUE);

if ( !file_exists($myConfig->getCreolePath()) )
	die('creole nicht gefunden');

define('CREOLE_BASEPATH', substr($myConfig->getCreolePath(), 0, strrpos($myConfig->getCreolePath(), '/')));

// workarounds
if ( substr($myConfig->getApplicationPath(), -1) != '/' )
	$myConfig->setApplicationPath($myConfig->getApplicationPath() . '/');
if ( substr($myConfig->getAbstractionPath(), -1) != '/' )
	$myConfig->setAbstractionPath($myConfig->getAbstractionPath() . '/');
if ( substr($myConfig->getSystemPath(), -1) != '/' )
	$myConfig->setSystemPath($myConfig->getSystemPath() . '/');
if ( substr($myConfig->getJsPath(), -1) != '/' )
	$myConfig->setJsPath($myConfig->getJsPath() . '/');

#if(!$myConfig->isValid())
#	die('Config muss vollständig sein.');

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
$myORMCompiler = new ORMCompiler($myConfig);