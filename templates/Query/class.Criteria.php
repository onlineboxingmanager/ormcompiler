<?php

/**
 * Class Criteria
 */
class Criteria
{

    public const EQUAL = '=';
    public const NOT_EQUAL = '<>';
    public const ALT_NOT_EQUAL = '!=';
    public const GREATER_THAN = '>';
    public const LESS_THAN = '<';
    public const GREATER_EQUAL = '>=';
    public const LESS_EQUAL = '<=';
    public const LIKE = 'LIKE';
    public const NOT_LIKE = 'NOT LIKE';
    public const CUSTOM = 'CUSTOM';
    public const DISTINCT = 'DISTINCT';
    public const IN = 'IN';
    public const NOT_IN = 'NOT IN';
    public const ALL = 'ALL';
    public const JOIN = 'JOIN';
    public const ASC = 'ASC';
    public const DESC = 'DESC';
    public const ISNULL = 'IS NULL';
    public const ISNOTNULL = 'IS NOT NULL';
    public const CURRENT_DATE = 'CURRENT_DATE';
    public const CURRENT_TIME = 'CURRENT_TIME';
    public const CURRENT_TIMESTAMP = 'CURRENT_TIMESTAMP';
    public const JOIN_LEFT = 'LEFT JOIN';
    public const JOIN_RIGHT = 'RIGHT JOIN';
    public const JOIN_INNER = 'INNER JOIN';
    public const LOGICAL_OR = 'OR';
    public const LOGICAL_AND = 'AND';

    private string $operator;
    private mixed $field = null;
    private mixed $value = null;
    private mixed $param = null;
    private ?string $table;
    private array $joins = [];
    private mixed $type = null;
    private string $stringchar = '"';
    private string $column;

    public function __construct(string $operator, string $column, mixed $value)
    {
        $this->operator = $operator;

        // set table and column
        $dotPos = strrpos($column, '.');

		if ($dotPos === false)
		{
            // no dot => aliased column
            $this->table = null;
            $this->column = $column;
        } else {
            $this->table = substr($column, 0, $dotPos);
            $this->column = substr($column, $dotPos + 1);
        }
        $this->field = $column;

        // value hinzufügen wenn nicht FK verknüpfung
        $this->value = $value;

        if( in_array($this->operator, [Criteria::IN, Criteria::NOT_IN]) AND is_array($value) )
        {
            foreach( $value as $key => $val )
            {
                if( !is_numeric($val) )
                    $value[$key] = '\'' . $val . '\'';
            }

            $this->value = '('.implode(',', $value).')';
        }

		elseif ( $value instanceof UUID )
		{
			$this->value = '?'; // default null
			if( !is_null($value->getValue()) )
				$this->value = 'UUID_TO_BIN(?)';

			$this->param = $value->getValue();
		}

        elseif ( !is_null($value) AND !preg_match('/^(tbl_)?[a-z\d\_]+\.[a-z\d\_]+$/ui', $value) AND !in_array($this->operator, array(Criteria::ISNOTNULL, Criteria::ISNULL, Criteria::IN, Criteria::NOT_IN)))
        {
            $this->value = '?';
            $this->param = $value;
        }
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getOperator(): string
    {
        return $this->operator;
    }

    public function getStringChar(): string
    {
        return $this->stringchar;
    }

    public function getParam(): string
    {
	    if( $this->param instanceof UUID )
			return $this->param->getValue();

        return $this->param;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function getWhereClause(): string
    {
	    #if( $this->value instanceof UUID )
		 #   return implode(" ", array($this->field, $this->getOperator(), $this->value->getValue()));

		if( in_array($this->getOperator(), [Criteria::ISNOTNULL, Criteria::ISNULL]) )
			return implode(" ", array($this->field, $this->getOperator()));
	    if( in_array($this->getOperator(), [Criteria::CUSTOM]) )
		    return implode(" ", array($this->field));

		return implode(" ", array($this->field, $this->getOperator(), $this->value));
    }

}