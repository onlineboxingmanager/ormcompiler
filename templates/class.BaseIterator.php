<?php
#ä

/**
 * Class BaseIterator
 */
class BaseIterator implements Iterator
{

	private int $position = 0;
	private array $array = array();

	public function __construct(array $array)
	{
		$this->position = 0;
		$this->array = $array;
	}

	#[ReturnTypeWillChange]
	function rewind()
	{
		$this->position = 0;
	}

	#[ReturnTypeWillChange]
	function current()
	{
		return $this->array[$this->position];
	}

	#[ReturnTypeWillChange]
	function key()
	{
		return $this->position;
	}

	#[ReturnTypeWillChange]
	function next()
	{
		++$this->position;
	}

	#[ReturnTypeWillChange] function valid(): bool
	{
		return isset($this->array[$this->position]);
	}
}