<?php
#ä

/**
 * base object class
 * @author MKaufmann
 */
abstract class BaseObject
{

	protected bool $is_new = true;
	protected bool $is_deleted = false;
	protected array $columns = [];
	protected array $references_loaded = [];
	//protected array $properties = [];
	//protected array $primarykeys = [];

	protected ?User $user_deleted;
	protected ?User $user_modified;
	protected ?User $user_created;

	protected ?UUID $userid_created = null;
	protected ?UUID $userid_modified = null;
	protected ?UUID $userid_deleted = null;
	protected int $tstamp_created = 0;
	protected ?int $tstamp_modified = NULL;
	protected ?int $tstamp_deleted = null;

	protected string $tablename;

	public function __construct()
	{
		foreach( $this->getProperties() as $key => $property )
		{
			if( $property['typename'] === 'BINARY' )
				$this->setAttribute($key, new UUID());
		}
	}

	public function setAttribute(string $key, mixed $value): void
	{
		if( !$this->getProperty($key) )
			return;

		$attribute = $this->getProperty($key);

		if( $attribute['typename'] === 'INTEGER' AND is_numeric($value) )
			$value = (int) $value;

		if( $attribute['typename'] === 'DOUBLE' AND $value !== '' )
			$value = (double) $value;

		if( $attribute['typename'] === 'DATE' AND !is_null($value) AND $value !== '' )
			$value = (int) strtotime($value);

		if( $attribute['is_nullable'] AND $value === '' )
			$value = null;

		if( $attribute['typename'] === 'BINARY' AND $value AND is_string($value) AND UUID::isUuidString($value) )
			$value = new UUID($value);

		$setter = 'set' . str_replace('_', '', $key);
		$this->$setter($value);
		//$this->$key = $value; // über diesen Weg ändern wir direkt das Attribute (Setter ginge auch aber die können bereits überschrieben sein. Z.B. Passwort )
	}

	public function getAttribute(string $key): mixed
	{
		$getter = 'get' . str_replace('_', '', $key);
		return $this->$getter();

		return $this->$key;
	}

	public function hasAttribute(string $key): mixed
	{
		return (bool) $this->getProperty($key);
	}

	/*
    public function getValue(string $key): mixed
    {
        return $this->getAttribute($key);
    }
	*/

	public function getRelation(string $name): mixed
	{
		$name = strtolower($name);
		$ref_name = 'ref_' . $name;

		if( isset($this->references_loaded[$ref_name]) )
			return $this->$ref_name;

		return false;
	}

	public function getProperty(string $property): array|bool
	{
		return $this->getProperties()[$property] ?? false;
	}

	public function getProperties(): array
	{
		if( 1 == 1 OR empty($this->properties) )
		{
			$classname = get_class($this);
			$myDAO = $classname . 'DAO';

			// get dao class
			Library::requireLibrary(LibraryKeys::ABSTRACTION_DAO_GENERIC($classname));

			//$this->properties = $myDAO::getProperties();
			//$this->primarykeys = $myDAO::getPrimaryKeys();

			return $myDAO::getProperties();
		}

		return $this->properties;
	}

	public function _getIsNew(): bool
	{
		return $this->is_new;
	}

	public function _setIsNew(bool $bool): void
	{
		$this->is_new = $bool;
	}

	public function _getIsDeleted(): bool
	{
		return $this->is_deleted;
	}

	public function _setIsDeleted(bool $bool): void
	{
		$this->is_deleted = $bool;
	}

	public function _setModified(string $column): void
	{
		$this->columns[$column] = true;
	}

	public function _getIsModified($column): bool
	{
		return isset($this->columns[$column]) AND $this->columns[$column] === true;
	}

	public function _getModified(): array
	{
		$myArray = array();
		foreach ( $this->columns as $col => $value )
			if ( $value === true )
			{
				$myArray[] = $col;
			}

		return $myArray;
	}

	public function _clearModifies(): void
	{
		foreach ( $this->columns as $col => $value )
			$this->columns[$col] = false;
	}

	public function _checkForModify($old, $new): bool
	{
		return ($old != $new);
	}

	public function _getIsLoaded(string $ref): mixed
	{
		return $this->references_loaded[$ref] ?? false;
	}

	public function _setIsLoaded(string $ref): void
	{
		$this->references_loaded[$ref] = true;
	}

	public function _clearLoaded(): void
	{
		$this->references_loaded = array();
	}

	public function getTimeToString($key): void
	{
		$time = $this->$key;
	}

	public function getPrimaryKey(): string
	{
		$keys = array_keys($this->getPrimaryKeys());
        if( empty($keys) )
            dd($this);
		return $keys[0];
	}

	public function getPrimaryKeys(): array
	{
		$classname = get_class($this);
		$myDAO = $classname . 'DAO';

		// get dao class
		Library::requireLibrary(LibraryKeys::ABSTRACTION_DAO_GENERIC($classname));

		//$this->properties = $myDAO::getProperties();
		//$this->primarykeys = $myDAO::getPrimaryKeys();

		return $myDAO::getPrimaryKeys();
	}

	public function getDefaultname(): string
	{
		$output = [];
		$output['pk'] = $this->getUid();
		foreach($this->getProperties() as $attributename => $attribute )
		{
			if( $attribute['typename'] !== 'INTEGER' AND $attributename == $this->getPrimaryKey() )
				break;

			if( $attribute['typename'] == 'INTEGER' AND $attributename == $this->getPrimaryKey() )
				continue;

			$attributename_getter = 'get' . str_replace('_', '' , $attributename);
			$value = $this->$attributename_getter();

			if( in_array($attribute['typename'], ['VARCHAR', 'TEXT']) )
			{
				if( preg_match('/^[A-Z0-9\_]{4,}$/u', $value) )
					$value = getTrans($value, $value);

				$output[] = $value;
				break;
			}
		}

		if( count($output) > 1 )
			unset($output['pk']);

		return implode(' - ', $output);
	}

	public function getUid(): UUID|int|string
	{
		return $this->getAttribute($this->getPrimaryKey());
	}

	#[\ReturnTypeWillChange]
	public static function where(string $field, mixed $operator = null, mixed $value = null): BaseQuery
	{
		$class = static::class;
		$class = $class . 'Query';
		$myBuilder = new $class();
		#if( strlen($field) > 1 AND !stristr($field, '.') )
		#	$field = $myBuilder->getTablename() . '.' . $field;
		$myBuilder->add($field, $operator, $value);
		return $myBuilder;
	}

	#[\ReturnTypeWillChange]
	public static function find(mixed $value = null): self|null
	{
		$class = static::class;
		$reflectionClass = new $class;
		$class = $class . 'Query';
		$property = $reflectionClass->getProperty($reflectionClass->getPrimaryKey());

		// TODO: performance - preg_match is slow
		if( is_string($value) AND $property['typename'] === 'BINARY' AND Uuid::isUuidString($value) )
			$value = new UUID($value);

		if( is_numeric($value) )
			$value = (int) $value;

		$myBuilder = new $class();
		$myBuilder->add($myBuilder->getTablename() . '.' . $reflectionClass->getPrimaryKey(), Criteria::EQUAL, $value);
		return $myBuilder->findOne();
	}

	#[\ReturnTypeWillChange]
	public static function all(): BaseQuery
	{
		return self::where('1', 1);
	}

	public function save( $is_force_save = false )
	{
		if( $this->_getIsNew() AND $this->getProperty('tstamp_created') )
		{
			$this->setTstampCreated(time());
			if( !empty($_ENV['user']) )
				$this->setUseridCreated($_ENV['user']->getUid());
		}

		if( !$this->_getIsNew() AND $this->getProperty('tstamp_modified') )
		{
			$this->setTstampModified(time());

			if( !empty($_ENV['user']) )
				$this->setUseridModified($_ENV['user']->getUid());
		}

		if( !$is_force_save AND count($this->_getModified()) == 0 )
			return false; // return if no changes was made

		$is_new = $this->_getIsNew();

		// create new uuid
		$uuid = null;
		if( $is_new AND $this->getProperty($this->getPrimaryKey())['typename'] === 'BINARY' AND is_null($this->getUid()->getValue()) AND $this->getProperty($this->getPrimaryKey())['is_nullable'] === false )
		{
			$uuid = new UUID(UUID::generate());
			$this->setAttribute($this->getPrimaryKey(), $uuid);
		}

		$id = (int) $this->_store();
		if( $uuid )
			$id = $uuid;

		$primarykeyname = 'set' . $this->getPrimaryKey();
		if( $is_new AND $this->getProperty($this->getPrimaryKey())['typename'] === 'INTEGER' )
			$this->$primarykeyname($id);

		$this->_setIsNew(false);

		return $id;
	}

	public function delete( ?bool $is_force_delete = false )
	{
		if( $is_force_delete )
		{
			if( $this->getUid() instanceof UUID )
				BaseDAO::genericQuery('DELETE FROM '.$this->getTablename().' WHERE '.$this->getPrimaryKey().' = UUID_TO_BIN(?)', array($this->getUid()?->getValue()));
			else
				BaseDAO::genericQuery('DELETE FROM '.$this->getTablename().' WHERE '.$this->getPrimaryKey().' = ?', array($this->getUid()));

			return true;
		}

		if( $this->getProperty('tstamp_deleted') )
			$this->setTstampDeleted(time());

		if( !empty($_ENV['user']) )
			$this->setUseridDeleted($_ENV['user']->getUid());

		$this->_setIsDeleted(true);

		return $this->save();
	}

	protected function _store()
	{
		$classname = get_class($this);
		$myDAO = $classname . 'DAO';

		// get dao class
		Library::requireLibrary(LibraryKeys::ABSTRACTION_DAO_GENERIC($classname));

		// store with dao
		return $myDAO::store($this);
	}

	public function getUserDeleted()
	{
		if( !empty($this->user_deleted) )
			return $this->user_deleted;

		if( !$this->getUseridDeleted() )
			return new User();

		Library::requireLibrary(LibraryKeys::APPLICATION_USER());
		$this->user_deleted = User::where('userid', $this->getUseridDeleted())->findOne();
		return $this->user_deleted;
	}

	public function getUserModified()
	{
		if( !empty($this->user_modified) )
			return $this->user_modified;

		if( !$this->getUseridModified() )
			return new User();

		Library::requireLibrary(LibraryKeys::APPLICATION_USER());
		$this->user_modified = User::where('userid', $this->getUseridModified())->findOne();
		return $this->user_modified;
	}

	public function getUserCreated()
	{
		if( isset($this->user_created) )
			return $this->user_created;

		Library::requireLibrary(LibraryKeys::APPLICATION_USER());
		$this->user_created = User::where('userid', $this->getUseridCreated())->findOne();
		return $this->user_created;
	}

	public function getTablename(): string
	{
		if( empty($this->tablename) )
		{
			$classname = get_class($this);
			$myDAO = $classname . 'DAO';

			// get dao class
			Library::requireLibrary(LibraryKeys::ABSTRACTION_DAO_GENERIC($classname));

			$this->tablename = $myDAO::TABLENAME;
		}

		return $this->tablename;
	}

	public function toArray(bool $include_tstamps_and_users = true): array
	{
		$output = [];
		foreach( $this->getProperties() as $key => $property )
		{
			$output[$key] = $this->getAttribute($key);

			if( $output[$key] instanceof UUID )
				$output[$key] = $output[$key]->getValue();
		}

		foreach ( $this->references_loaded as $key => $reference )
		{
			$output[$key] = $this->$key->toArray($include_tstamps_and_users);
		}

		if( isset($output['password']) )
			$output['password'] = '';

		if( !$include_tstamps_and_users )
		{
			unset($output['tstamp_created']);
			unset($output['tstamp_modified']);
			unset($output['tstamp_deleted']);
			unset($output['userid_created']);
			unset($output['userid_modified']);
			unset($output['userid_deleted']);
		}

		return $output;
	}

	public function setTstampCreated( int $int): void
	{
		if( $this->tstamp_created !== $int )
		{
			$this->tstamp_created = $int;
			$this->_setModified('tstamp_created');
		}
	}

	public function getTstampCreated(): int
	{
		return $this->tstamp_created;
	}

	public function setTstampModified( ?int $int): void
	{
		if( $this->tstamp_modified !== $int )
		{
			$this->tstamp_modified = $int;
			$this->_setModified('tstamp_modified');
		}
	}

	public function getTstampModified(): ?int
	{
		return $this->tstamp_modified;
	}

	public function setTstampDeleted( ?int $int): void
	{
		if( $this->tstamp_deleted !== $int )
		{
			$this->tstamp_deleted = $int;
			$this->_setModified('tstamp_deleted');
		}
	}

	public function getTstampDeleted(): ?int
	{
		return $this->tstamp_deleted;
	}

	public function setUseridCreated( ?uuid $UUID): void
	{
		if( $this->userid_created !== $UUID )
		{
			$this->userid_created = $UUID;
			$this->_setModified('userid_created');
		}
	}

	public function getUseridCreated(): ?uuid
	{
		return $this->userid_created;
	}

	public function setUseridModified( ?uuid $UUID): void
	{
		if( $this->userid_modified !== $UUID )
		{
			$this->userid_modified = $UUID;
			$this->_setModified('userid_modified');
		}
	}

	public function getUseridModified(): ?uuid
	{
		return $this->userid_modified;
	}

	public function setUseridDeleted( ?uuid $UUID): void
	{
		if( $this->userid_deleted !== $UUID )
		{
			$this->userid_deleted = $UUID;
			$this->_setModified('userid_deleted');
		}
	}

	public function getUseridDeleted(): ?uuid
	{
		return $this->userid_deleted;
	}

}

class UUID
{
	protected ?string $value = null;

	public function __construct(?string $value = null)
	{
		$this->setValue($value);
	}

	public function getValue(): ?string
	{
		return $this->value;
	}

	public function setValue( ?string $value ): void
	{
		$this->value = $value;
	}

	public static function generate()
	{
		// Generiere eine Version 4 UUID
		$data = openssl_random_pseudo_bytes(16);
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40); // Version 4
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80); // Variant RFC 4122

		// Formatieren der UUID
		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}

	public static function isUuidString(string $uuid): bool
	{
		return preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid);
	}
}